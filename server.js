// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
var request = require('request');
var formData = require('form-data');
var fs = require('fs');
var multer  = require('multer');
var logger = require('connect-logger');
var cookieParser = require('cookie-parser');
var session = require('cookie-session');
var crypto = require('crypto');
var assert = require('assert-plus');
var bunyan = require('bunyan');
var passport = require('passport');
var BearerStrategy = require('passport-azure-ad').BearerStrategy;

// Get our API routes
const api = require('./server/routes/api');
var config = require('./appConfig');

const app = express();

// Azure AD integration - Web Client Security
var AuthenticationContext = require('adal-node').AuthenticationContext;

var clientId = config.azureAppId;
var clientSecret = config.azureClientKey;
var authorityHostUrl = config.azureAuthorityHostUrl;
var tenant = config.azureTentantDomainName;
var authorityUrl = authorityHostUrl + '/' + tenant;

if (process.env.NODE_ENV == 'dev') {
    var redirectUri = config.devDNSName + ':' + config.devPort + '/getAToken';
} else {
    var redirectUri = config.nodeDNSName + ':' + config.nodePort + '/getAToken';
}

var resource = config.azureResourceSPNId;
var templateAuthzUrl = 'https://login.windows.net/' + tenant + '/oauth2/authorize?response_type=code&client_id=' + clientId + '&redirect_uri=' + redirectUri + '&state=<state>&resource=' + resource;

app.use(logger());
app.use(cookieParser('a deep secret'));
app.use(session({secret: '1234567890QWERTY'}));

// Azure AD integration - Node API security
var options = {
 identityMetadata: 'https://login.microsoftonline.com/' + config.azureTentantId + '/.well-known/openid-configuration',
 clientID: config.azureAppId,
 validateIssuer: true,
 audience: 'spn:' + config.azureResourceSPNId,
 passReqToCallback: false,
 loggingLevel: 'info'
};

// Array to hold logged in users and the current logged in user (owner).
var users = [];
var owner = null;

// Our logger.
var log = bunyan.createLogger({
 name: 'Azure Active Directory Bearer Sample',
      streams: [
     {
         stream: process.stderr,
         level: "error",
         name: "error"
     },
     {
         stream: process.stdout,
         level: "warn",
         name: "console"
     }, ]
});

// If the logging level is specified, switch to it.
if (options.loggingLevel) { log.levels("console", options.loggingLevel); }

// Let's start using Passport.js
app.use(passport.initialize()); // Starts passport
app.use(passport.session()); // Provides session support

var findById = function(id, fn) {
    for (var i = 0, len = users.length; i < len; i++) {
        var user = users[i];
        if (user.sub === id) {
            log.info('Found user: ', user);
            return fn(null, user);
        }
    }
    return fn(null, null);
};

var bearerStrategy = new BearerStrategy(options,
    function(token, done) {
        log.info('verifying the user');
        log.info(token, 'was the token retreived');
        findById(token.sub, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                // "Auto-registration"
                log.info('User was added automatically as they were new. Their sub is: ', token.sub);
                users.push(token);
                owner = token.sub;
                return done(null, token);
            }
            owner = token.sub;
            return done(null, user, token);
        });
    }
);

passport.use(bearerStrategy);

// Parsers for POST data
//app.use(bodyParser.json());
app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({limit: '10mb',extended: true }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
// app.use('/api', api);
app.use('/api', passport.authenticate('oauth-bearer', { session: false }), api);


// Clients get redirected here in order to create an OAuth authorize url and redirect them to AAD.
// There they will authenticate and give their consent to allow this app access to
// some resource they own.

function createAuthorizationUrl(state) {
  return templateAuthzUrl.replace('<state>', state);
}

app.get('/auth', function(req, res) {
  crypto.randomBytes(48, function(ex, buf) {
    var token = buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-');

    res.cookie('authstate', token);
    var authorizationUrl = createAuthorizationUrl(token);

	res.redirect(authorizationUrl);
  });
});


// After consent is granted AAD redirects here.  The ADAL library is invoked via the
// AuthenticationContext and retrieves an access token that can be used to access the
// user owned resource.
app.get('/getAToken', function(req, res) {
	if (req.cookies.authstate !== req.query.state) {
		res.send('error: state does not match');
	}

	var authenticationContext = new AuthenticationContext(authorityUrl);

	authenticationContext.acquireTokenWithAuthorizationCode(
		req.query.code,
		redirectUri,
		resource,
		clientId,
		clientSecret,
		function(err, response) {
		  var errorMessage = '';
		  if (err) {
			errorMessage = 'error: ' + err.message + '\n';
			console.log(JSON.stringify(errorMessage));
			res.redirect(config.nodeDNSName);
		  }
		  errorMessage += 'response: ' + JSON.stringify(response);
		  res.cookie('accessToken',response.accessToken);

      if (process.env.NODE_ENV == 'dev') {
          res.redirect(config.devDNSName + ':' + config.devPort + '/landing');
      } else {
          res.redirect(config.nodeDNSName  + ':' + config.nodePort + '/landing');
      }		            
		});
	});


// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/**
 * Get port from environment and store in Express.
 */
var port = config.nodePort;

if (process.env.NODE_ENV == 'dev') {
    port = config.devPort;
}

app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));
server.timeout = 1000000;
