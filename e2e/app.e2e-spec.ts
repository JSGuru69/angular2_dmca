import { DmcaUiPage } from './app.po';

describe('dmca-ui App', function() {
  let page: DmcaUiPage;

  beforeEach(() => {
    page = new DmcaUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
