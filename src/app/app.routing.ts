import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from "./shared/login/login.component";
import { LandingComponent } from './landing-page/landing.component';
import { TeamDashboardComponent } from "./dcma-team/dashboard/team-dashboard.component";
import { OGCDashboardComponent } from "./ogc-business/ogc-dashboard/ogc-dashboard.component";
import { SubmissionComponent } from "./content-owner/submission/submission.component";
import { ManageComponent } from "./dmca-admin/manage/manage.component";
import { APIsComponent } from "./dmca-admin/apis/apis.component";
import { CreateRuleComponent } from "./dmca-admin/create-rule/create-rule.component";
import { CreateAPIComponent } from "./dmca-admin/create-api/create-api.component";
import { ManageSideNavComponent } from "./dmca-admin/manage-sidenav/manage-sidenav.component";
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { BulkUploadComponent } from './content-owner/bulk-upload/bulk-upload.component';
import { ManualSubmissionComponent } from './content-owner/manual-submission/manual-submission.component';
import { LoginPageComponent } from './login-page/login-page.component';

const appRoutes: Routes = [
  { path: '', component: LoginPageComponent },
  { path: 'landing', component: LandingComponent },
  { path: 'dashboard', component: TeamDashboardComponent },
  { path: 'ogc-dashboard', component: OGCDashboardComponent },
  { path: 'submission', component: SubmissionComponent },
  { path: 'manage', component: ManageComponent },
  { path: 'apis', component: APIsComponent },
  { path: 'create-rule', component: CreateRuleComponent },
  { path: 'create-api', component: CreateAPIComponent },
  { path: 'bulkupload', component: BulkUploadComponent },
  { path: 'manual-submission', component: ManualSubmissionComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  providers: []
})
export class DmcaRouting { }