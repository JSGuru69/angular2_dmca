import { Component, AfterViewInit, Attribute, Output, EventEmitter } from '@angular/core';
import { InputFilter } from '../../services/query.service';
import { API } from '../../http/http.service';
//import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { Http, Headers } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';
import { environment } from './../../../environments/environment';
import { Cookie } from 'ng2-cookies';

const appConfig = require('../../../../appConfig.js');
//import { Ng2AutoCompleteModule } from 'ng2-auto-complete';
//import {MdDialog, MdDialogRef, MdSnackBar} from '@angular/material';

@Component({
    selector: 'team-dashboard',
    templateUrl: './team-dashboard.component.html',
    styleUrls: ['../../main.scss'],
    providers: [InputFilter, API]
})
export class TeamDashboardComponent implements AfterViewInit {
    private dashboard = require("assets/dashboard.png");
    dayLineOption: Object;
    businessLine: any;
    chartOne: any;
    chartTwo: any;
    chartThree: any;
    businessBar: any;

    totalWidth: any;
    dayBarOption: Object;
    dayBarWidth: any;
    dayBarStatus: any;
    filter: any;
    searchItem: Boolean = true;
    historyOption: Object;
    historyWidth: any;
    violationDTD: Object;
    violationQTD: Object;
    violationMTD: Object;
    violationYTD: Object;
    contentType: Object;
    methodViolationYTD: Object;
    methodViolationQTD: Object;
    methodViolationMTD: Object;
    methodViolationDTD: Object;
    totalUploadsYTD: Object;
    totalUploadsQTD: Object;
    totalUploadsMTD: Object;
    totalUploadsDTD: Object;
    noticesChartYTD: Object;
    noticesChartDTD: Object;
    noticesChartMTD: Object;
    noticesChartQTD: Object;

    businessCount: number = 0;
    residentialCount: number = 0;
    totalCount: number = 0;
    loading: boolean = false;
    methodVio: Array<any>;
    totalNoticeWidth: any;
    searchName: any;
    searchItems: Array<any>;
    searchProgress: Boolean = false;
    notFound: Boolean = false;
    responseLength: any;
    violationList: Array<any> = [];
    downloadList: Array<any> = [];
    infringementDetails: any;

    dtdTotalNotices: any = {};
    mtdTotalNotices: any = {};
    qtdTotalNotices: any = {};
    ytdTotalNotices: any = {};
    ytdSelected: Boolean = false;
    qtdSelected: Boolean = false;
    mtdSelected: Boolean = false;
    dtdSelected: Boolean = false;

    contentOwnerYTD: Array<any> = [];
    contentOwnerDTD: Array<any> = [];
    contentOwnerQTD: Array<any> = [];
    contentOwnerMTD: Array<any> = [];
    private environmentName = environment.name;

    @Output() searchEvent: EventEmitter<any> = new EventEmitter<any>();
    vioBtn: Boolean = true;

    constructor(private httpservice: API, private http: Http) { //private _service: NotificationsService,) {
    };

    autoComplete(value) {
        let self = this;
        self.searchItems = [];
        const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : 'Bearer ' + Cookie.get('accessToken') });
        self.notFound = false;
        var searchString = value.toLowerCase();
        let url;
        if (self.environmentName === 'dev')
            url = appConfig.devUri + 'autocomplete/' + searchString;
        if (self.environmentName === 'prod')
            url = appConfig.prodUri + 'autocomplete/' + searchString;   
        if (value.length > 2) {
            console.log('search value', searchString);
            let searchReponse;
            let searchLength;
            self.searchProgress = true;
            self.http.get(url, { headers: headers }).toPromise().then((data) => {
                console.log("data before json", data)
                searchReponse = data.json();
                searchLength = data;
                self.responseLength = searchLength._body;
                if (self.responseLength === '{}') {
                    self.notFound = true;
                    self.searchProgress = false;
                }
                else {
                    console.log('response not empty');
                    let response = searchReponse.hits.hits;
                    let searchObj = [];
                    for (var i = 0; i < response.length; i++) {
                        searchObj.push({
                            id: response[i]._source.id,
                            name: response[i]._source.name,
                            address: response[i]._source.address,
                            level: response[i]._source.level,
                            email: response[i]._source.email,
                            house_number: response[i]._source.house_number,
                            member_since: response[i]._source.member_since,
                            state: response[i]._source.state,
                            street_name: response[i]._source.street_name,
                            type: response[i]._source.type,
                            wan_ip: response[i]._source.wan_ip,
                            zipcode: response[i]._source.zipcode,
                            dns_primary: response[i]._source.dns_primary,
                            status: response[i]._source.status
                        })
                    }
                    self.searchItems = searchObj;
                    self.notFound = false;
                    console.log('searchItems', self.searchItems);
                }
                self.searchProgress = false;
            }).catch((error) => { console.log(error) });
        }
    }

    subscriberDetails: any;
    subscriberId: any;
    violationCount: any = 0;
    subscriberInfrigements: any;
    status: any;
    onSubscriberSelected(selectedItem) {
        let self = this;
        self.loading = true;
        self.subscriberDetails = selectedItem;
        let date = new Date(self.subscriberDetails.member_since);
        self.subscriberDetails.joining_year = date.getFullYear();
        // //Form the Subscriber details API URL
        self.subscriberId = selectedItem.id;
        self.status = (selectedItem.status).toLowerCase();
        let url = "subscriberselected/" + self.subscriberId;
        self.httpservice.fetchData('get', url, (data) => {
            var response = JSON.parse(data._body);
            var subscriberDetails = response.subscriberDetails;
            var violationhistory = response.violationhistory;
            this.loadInfrigementDetails(subscriberDetails.hits.hits);
            this.loadViolationHistory(violationhistory.hits.hits);
        }, (error) => { console.log(error) })
    }

    loadInfrigementDetails(results) {
        let self = this;
        self.subscriberInfrigements = results;
        self.violationCount = 0;

        let l1Json: any; let l2Json: any; let l3Json: any; let l4Json: any; let l5Json: any; let l6Json: any; let l7Json: any; let l8Json: any;
        for (var i in self.subscriberInfrigements) {
            switch (self.subscriberInfrigements[i]._source.infringement_level) {
                case 1:
                    self.subscriberInfrigements[i]._source.showInfringementDetails = false;
                    self.subscriberInfrigements[i]._source.downgradeList = [0];
                    //filmDetail
                    if (l1Json == undefined) {
                        l1Json = { "level": "Level 1", "infrigements": self.subscriberInfrigements[i]._source.infringements, "showViolationDetails": false, "infrigements_list": [self.subscriberInfrigements[i]._source] }
                    } else {
                        //l1Json.infrigements = l1Json.infrigements+self.subscriberInfrigements[i].infringements;
                        l1Json.infrigements_list.push(self.subscriberInfrigements[i]._source);
                    }
                    self.vioBtn = true;
                    break;
                case 2:
                    self.subscriberInfrigements[i]._source.showInfringementDetails = false;
                    self.subscriberInfrigements[i]._source.downgradeList = [1, 0];
                    if (l2Json == undefined) {
                        l2Json = { "level": "Level 2", "infrigements": self.subscriberInfrigements[i]._source.infringements, "showViolationDetails": false, "infrigements_list": [self.subscriberInfrigements[i]._source] }

                    } else {
                        //l1Json.infrigements = l1Json.infrigements+self.subscriberInfrigements[i].infringements;
                        l2Json.infrigements_list.push(self.subscriberInfrigements[i]._source);
                    }
                    self.vioBtn = true;
                    break;
                case 3:
                    self.subscriberInfrigements[i]._source.showInfringementDetails = false;
                    self.subscriberInfrigements[i]._source.downgradeList = [2, 1, 0];
                    if (l3Json == undefined) {
                        l3Json = { "level": "Level 3", "infrigements": self.subscriberInfrigements[i]._source.infringements, "showViolationDetails": false, "infrigements_list": [self.subscriberInfrigements[i]._source] }
                    } else {
                        //l1Json.infrigements = l1Json.infrigements+self.subscriberInfrigements[i].infringements;
                        l3Json.infrigements_list.push(self.subscriberInfrigements[i]._source);
                    }
                    self.vioBtn = true;
                    break;
                case 4:
                    self.subscriberInfrigements[i]._source.showInfringementDetails = false;
                    self.subscriberInfrigements[i]._source.downgradeList = [3, 2, 1, 0];
                    if (l4Json == undefined) {
                        l4Json = { "level": "Level 4", "infrigements": self.subscriberInfrigements[i]._source.infringements, "showViolationDetails": false, "infrigements_list": [self.subscriberInfrigements[i]._source] }
                    } else {
                        //l1Json.infrigements = l1Json.infrigements+self.subscriberInfrigements[i].infringements;
                        l4Json.infrigements_list.push(self.subscriberInfrigements[i]._source);
                    }
                    self.vioBtn = true;
                    break;
                case 5:
                    self.subscriberInfrigements[i]._source.showInfringementDetails = false;
                    self.subscriberInfrigements[i]._source.downgradeList = [4, 3, 2, 1, 0];
                    if (l5Json == undefined) {
                        l5Json = { "level": "Level 5", "infrigements": self.subscriberInfrigements[i]._source.infringements, "showViolationDetails": false, "infrigements_list": [self.subscriberInfrigements[i]._source] }
                    } else {
                        //l1Json.infrigements = l1Json.infrigements+self.subscriberInfrigements[i].infringements;
                        l5Json.infrigements_list.push(self.subscriberInfrigements[i]._source);
                    }
                    self.vioBtn = true;
                    break;
                case 6:
                    self.subscriberInfrigements[i]._source.showInfringementDetails = false;
                    self.subscriberInfrigements[i]._source.downgradeList = [5, 4, 3, 2, 1, 0];
                    if (l6Json == undefined) {
                        l6Json = { "level": "Level 6", "infrigements": self.subscriberInfrigements[i]._source.infringements, "showViolationDetails": false, "infrigements_list": [self.subscriberInfrigements[i]._source] }
                    } else {
                        //l1Json.infrigements = l1Json.infrigements+self.subscriberInfrigements[i].infringements;
                        l6Json.infrigements_list.push(self.subscriberInfrigements[i]._source);
                    }
                    self.vioBtn = true;
                    break;
                case 7:
                    self.subscriberInfrigements[i]._source.showInfringementDetails = false;
                    self.subscriberInfrigements[i]._source.downgradeList = [6, 5, 4, 3, 2, 1, 0];
                    if (l7Json == undefined) {
                        l7Json = { "level": "Level 7", "infrigements": self.subscriberInfrigements[i]._source.infringements, "showViolationDetails": false, "infrigements_list": [self.subscriberInfrigements[i]._source] }
                    } else {
                        //l1Json.infrigements = l1Json.infrigements+self.subscriberInfrigements[i].infringements;
                        l7Json.infrigements_list.push(self.subscriberInfrigements[i]._source);
                    }
                    self.vioBtn = false;
                    break;
                case 8:
                    self.subscriberInfrigements[i]._source.showInfringementDetails = false;
                    self.subscriberInfrigements[i]._source.downgradeList = [7, 6, 5, 4, 3, 2, 1, 0];
                    if (l8Json == undefined) {
                        l8Json = { "level": "Level 8", "infrigements": self.subscriberInfrigements[i]._source.infringements, "showViolationDetails": false, "infrigements_list": [self.subscriberInfrigements[i]._source] }
                    } else {
                        //l1Json.infrigements = l1Json.infrigements+self.subscriberInfrigements[i].infringements;
                        l8Json.infrigements_list.push(self.subscriberInfrigements[i]._source);
                    }
                    self.vioBtn = false;
                    break;
            }
        }
        self.violationList = [];
        if (l8Json != undefined) self.violationList.push(l8Json);
        if (l7Json != undefined) self.violationList.push(l7Json);
        if (l6Json != undefined) self.violationList.push(l6Json);
        if (l5Json != undefined) self.violationList.push(l5Json);
        if (l4Json != undefined) self.violationList.push(l4Json);
        if (l3Json != undefined) self.violationList.push(l3Json);
        if (l2Json != undefined) self.violationList.push(l2Json);
        if (l1Json != undefined) self.violationList.push(l1Json);
        for(var entry in self.violationList){
            self.violationCount += parseInt(self.violationList[entry].infrigements);
        }
        console.log("Violation List: ", self.violationList);

    }

    loadViolationHistory(results) {
        let self = this;
        let data = [];
        var vioDate;
        for (var item in results) {
            vioDate = new Date(results[item]._source.notification_date);
            var d = Date.UTC(vioDate.getFullYear(), vioDate.getMonth(), vioDate.getDate())
            let dataItem = [d, results[item]._source.infringement_level];
            console.log(dataItem);
            data.push(dataItem);
        }
        console.log(data);
        self.historyOption = {
            chart: { type: 'line', backgroundColor: '#494B62', width: '273', height: '288' },
            colors: ['#FFC31F'],
            title: { text: 'Violation History', align: 'left', style: { "color": "#fff" } },
            xAxis: { type: 'datetime', labels: { enabled: false } },
            yAxis: { min: 1, max: 8, tickInterval: 1, title: { text: null }, labels: { enabled: true, style: { color: '#fff' } }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0, tickLength: 0 },
            credits: { enabled: false },
            plotOptions: { series: { marker: { enabled: true } } },
            series: [{ data: data, showInLegend: false }]
        };
        self.loading = false;
        self.searchItem = false;
        self.searchName = '';
    }

    public preViolation: number = -1;
    violationOpen(vioIndex) {// only show clicked img info 
        let self = this;
        if (this.violationList[this.preViolation] && this.preViolation != vioIndex) {
            this.violationList[this.preViolation].showViolationDetails = false;
        }
        this.violationList[vioIndex].showViolationDetails = !this.violationList[vioIndex].showViolationDetails;
        this.preViolation = vioIndex;

        for (var entry in self.violationList[vioIndex].infrigements_list) {
            if (self.violationList[vioIndex].infrigements_list[entry].display_date == undefined) {
                var date = new Date(self.violationList[vioIndex].infrigements_list[entry].datestamp);
                self.violationList[vioIndex].infrigements_list[entry].display_date = date.toLocaleDateString('en-US', { timeZone: 'UTC' });
            }
        }
    };
    public filmDetail: Boolean = false;
    public prevFilm: number = -1;
    filmOpen(index, i) {// only show clicked img info 
        let self = this;
        var selectedInfrg = self.violationList[i].infrigements_list[index];
        var date1 = new Date(selectedInfrg.abuse_date);
        selectedInfrg.display_abuse_date = date1.toLocaleDateString('en-US', { timeZone: 'UTC' });
        var date2 = new Date(selectedInfrg.received_date);
        selectedInfrg.display_received_date = date2.toLocaleDateString('en-US', { timeZone: 'UTC' });
        if(self.violationList[i].infrigements_list[index].status == "Acknowledged" || self.violationList[i].infrigements_list[index].status == "Disputed") 
            self.violationList[i].infrigements_list[index].actionComplete = true;
        else
            self.violationList[i].infrigements_list[index].actionComplete = false;
        if (self.violationList[i].infrigements_list[this.prevFilm] && this.prevFilm != index) {
            self.violationList[i].infrigements_list[this.prevFilm].showInfringementDetails = false;
        }
        selectedInfrg.showInfringementDetails = !selectedInfrg.showInfringementDetails;
        //self.violationList[i].infrigements_list[index].showInfringementDetails = !self.violationList[i].infrigements_list.showInfringementDetails;
        this.prevFilm = index;
    };

    acknowledgeClick(item) {
        var self = this;
        self.loading = true;
        var url = 'subscriber/action';
        var jsonbody = {
            "infringement_key": item.infringement_key,
            "user_action": "acknowledge"
        }

        self.httpservice.fetchData('post', url, (data) => {
            var resp = JSON.parse(data._body)
            if (resp[0].message == "success") {
                item.actionComplete = true;
                item.status = "Acknowledged"
            }
            else{
                console.log("Error in acknowledging infrigement. Please try again later!");    
            }
            self.loading = false;
        }, (error) => { 
            console.log(error); 
            self.loading = false; 
        }, jsonbody)
    }

    ngAfterViewInit() {
        let self = this;
        self.loading = true;
        var elem = document.querySelector('.business-chart').clientWidth;
        self.totalWidth = elem;
        console.log('elem', elem);
        if (elem > 700) {
            self.businessLine = (elem / 2.5) - 30;
            self.chartOne = elem / 2.5;
            self.totalNoticeWidth = (elem - self.chartOne) - 30;
            console.log('businessLine', self.businessLine);
            self.businessBar = ((elem - self.businessLine) / 2) - 30;
            self.chartTwo = (elem - self.businessLine) / 2.5;
            self.chartThree = self.chartTwo - 45;
            console.log('businessBar', self.businessBar);
            self.dayBarWidth = self.chartTwo / 3.5;
            self.dayBarStatus = self.chartTwo - self.dayBarWidth;
        }
        else {
            self.businessLine = elem - 30;
            self.chartOne = elem;
            self.totalNoticeWidth = elem;
            console.log('businessLine', self.businessLine);
            self.businessBar = elem - 30;
            self.chartTwo = elem;
            self.chartThree = elem;
            console.log('businessBar', self.businessBar);
            self.dayBarWidth = self.businessBar / 3;
            self.dayBarStatus = self.businessBar - self.dayBarWidth;
        }
        self.dashboardAPICalls();
    }

    dashboardAPICalls() {
        let self = this;
        let url = "dashboard"
        self.httpservice.fetchData('get', url, (data) => {
            var response = JSON.parse(data._body);
            var serviceTypeYTD = JSON.parse(response.serviceTypeYTD);
            var serviceTypeQTD = JSON.parse(response.serviceTypeQTD);
            var serviceTypeMTD = JSON.parse(response.serviceTypeMTD);
            var serviceTypeDTD = JSON.parse(response.serviceTypeDTD);
            var totalNoticesYTD = JSON.parse(response.totalNoticesYTD);
            var totalNoticesDTD = JSON.parse(response.totalNoticesDTD);
            var totalNoticesQTD = JSON.parse(response.totalNoticesQTD);
            var totalNoticesMTD = JSON.parse(response.totalNoticesMTD);
            var totalNoticesChartYTD = JSON.parse(response.totalNoticesChartYTD);
            var totalNoticesChartDTD = JSON.parse(response.totalNoticesChartDTD);
            var totalNoticesChartMTD = JSON.parse(response.totalNoticesChartMTD);
            var totalNoticesChartQTD = JSON.parse(response.totalNoticesChartQTD);
            var totalUploadEventsYTD = JSON.parse(response.totalUploadEventsYTD);
            var totalUploadEventsQTD = JSON.parse(response.totalUploadEventsQTD);
            var totalUploadEventsMTD = JSON.parse(response.totalUploadEventsMTD);
            var totalUploadEventsDTD = JSON.parse(response.totalUploadEventsDTD);
            var violationYTD = JSON.parse(response.violationYTD);
            var violationQTD = JSON.parse(response.violationQTD);
            var violationMTD = JSON.parse(response.violationMTD);
            var violationDTD = JSON.parse(response.violationDTD);
            var contentOwnerYTD = JSON.parse(response.contentOwnerYTD);
            var contentOwnerQTD = JSON.parse(response.contentOwnerQTD);
            var contentOwnerMTD = JSON.parse(response.contentOwnerMTD);
            var contentOwnerDTD = JSON.parse(response.contentOwnerDTD);

            if (serviceTypeYTD.hasOwnProperty("hits")) self.loadServiceType(serviceTypeYTD.hits.hits, "Y");
            if (serviceTypeQTD.hasOwnProperty("hits")) self.loadServiceType(serviceTypeQTD.hits.hits, "Q");
            if (serviceTypeMTD.hasOwnProperty("hits")) self.loadServiceType(serviceTypeMTD.hits.hits, "M");
            if (serviceTypeDTD.hasOwnProperty("hits")) self.loadServiceType(serviceTypeDTD.hits.hits, "D");

            if (totalNoticesYTD.hasOwnProperty("hits")) self.loadTotalNotices(totalNoticesYTD.hits.hits, "Y");
            if (totalNoticesQTD.hasOwnProperty("hits")) self.loadTotalNotices(totalNoticesQTD.hits.hits, "Q");
            if (totalNoticesMTD.hasOwnProperty("hits")) self.loadTotalNotices(totalNoticesMTD.hits.hits, "M");
            if (totalNoticesDTD.hasOwnProperty("hits")) self.loadTotalNotices(totalNoticesDTD.hits.hits, "D");

            if (totalNoticesChartYTD.hasOwnProperty("hits")) self.loadNoticesChart(totalNoticesChartYTD.hits.hits, "Y");
            if (totalNoticesChartQTD.hasOwnProperty("hits")) self.loadNoticesChart(totalNoticesChartQTD.hits.hits, "Q");
            if (totalNoticesChartMTD.hasOwnProperty("hits")) self.loadNoticesChart(totalNoticesChartMTD.hits.hits, "M");
            if (totalNoticesChartDTD.hasOwnProperty("hits")) self.loadNoticesChart(totalNoticesChartDTD.hits.hits, "D");

            if (contentOwnerYTD.hasOwnProperty("hits")) self.loadContentOwner(contentOwnerYTD.hits.hits, "Y");
            if (contentOwnerQTD.hasOwnProperty("hits")) self.loadContentOwner(contentOwnerQTD.hits.hits, "Q");
            if (contentOwnerMTD.hasOwnProperty("hits")) self.loadContentOwner(contentOwnerMTD.hits.hits, "M");
            if (contentOwnerDTD.hasOwnProperty("hits")) self.loadContentOwner(contentOwnerDTD.hits.hits, "D");

            if (totalUploadEventsYTD.hasOwnProperty("hits")) self.loadTrialTotalUploads(totalUploadEventsYTD.hits.hits, "Y");
            if (totalUploadEventsQTD.hasOwnProperty("hits")) self.loadTrialTotalUploads(totalUploadEventsQTD.hits.hits, "Q");
            if (totalUploadEventsMTD.hasOwnProperty("hits")) self.loadTrialTotalUploads(totalUploadEventsMTD.hits.hits, "M");
            if (totalUploadEventsDTD.hasOwnProperty("hits")) self.loadTrialTotalUploads(totalUploadEventsDTD.hits.hits, "D");

            if (violationYTD.hasOwnProperty("hits")) self.loadViolationAggregate(violationYTD.hits.hits, "Y")
            if (violationQTD.hasOwnProperty("hits")) self.loadViolationAggregate(violationQTD.hits.hits, "Q")
            if (violationMTD.hasOwnProperty("hits")) self.loadViolationAggregate(violationMTD.hits.hits, "M")
            if (violationDTD.hasOwnProperty("hits")) self.loadViolationAggregate(violationDTD.hits.hits, "D")
            self.ytdSelected = true;
            self.loading = false;
        }, (error) => { console.log(error) })
    }

    loadViolationAggregate(results, type: String) {
        let level8Total = 0;
        let level1Total = 0;
        let level2Total = 0;
        let level3Total = 0;
        let level4Total = 0;
        let level5Total = 0;
        let level6Total = 0;
        let level7Total = 0;
        for (var i in results) {
            switch (results[i]._source.infringement_level_name) {
                case "Level 1":
                    level1Total = results[i]._source.total_notices;
                    break;
                case "Level 2":
                    level2Total = results[i]._source.total_notices;
                    break;
                case "Level 3":
                    level3Total = results[i]._source.total_notices;
                    break;
                case "Level 4":
                    level4Total = results[i]._source.total_notices;
                    break;
                case "Level 5":
                    level5Total = results[i]._source.total_notices;
                    break;
                case "Level 6":
                    level6Total = results[i]._source.total_notices;
                    break;
                case "Level 7":
                    level7Total = results[i]._source.total_notices;
                    break;
                case "Level 8":
                    level8Total = results[i]._source.total_notices;
                    break;
            }
        }

        if (type == "Y") {
            this.violationYTD = {
                chart: { type: 'column', height: '297', width: '120', style: { "fontFamily": "Roboto,sans-serif", "outline": "1px solid #D8D8D8" }, marginBottom: 35 },
                title: { text: 'VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#6E7191', '#404256'],
                xAxis: { categories: ['Violations'], labels: { enabled: false }, title: { text: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: false },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: [{
                    name: 'Level 8',
                    data: [level8Total]
                }, {
                    name: 'Level 7',
                    data: [level7Total]
                }, {
                    name: 'Level 6',
                    data: [level6Total]
                }, {
                    name: 'Level 5',
                    data: [level5Total]
                }, {
                    name: 'Level 4',
                    data: [level4Total]
                }, {
                    name: 'Level 3',
                    data: [level3Total]
                }, {
                    name: 'Level 2',
                    data: [level2Total]
                }, {
                    name: 'Level 1',
                    data: [level1Total]
                }]
            };
        }

        if (type == "Q") {
            this.violationQTD = {
                chart: { type: 'column', height: '297', width: '120', style: { "fontFamily": "Roboto,sans-serif", "outline": "1px solid #D8D8D8" }, marginBottom: 35 },
                title: { text: 'VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#6E7191', '#404256'],
                xAxis: { categories: ['Violations'], labels: { enabled: false }, title: { text: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: false },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: [{
                    name: 'Level 8',
                    data: [level8Total]
                }, {
                    name: 'Level 7',
                    data: [level7Total]
                }, {
                    name: 'Level 6',
                    data: [level6Total]
                }, {
                    name: 'Level 5',
                    data: [level5Total]
                }, {
                    name: 'Level 4',
                    data: [level4Total]
                }, {
                    name: 'Level 3',
                    data: [level3Total]
                }, {
                    name: 'Level 2',
                    data: [level2Total]
                }, {
                    name: 'Level 1',
                    data: [level1Total]
                }]
            };
        }

        if (type == "M") {
            this.violationMTD = {
                chart: { type: 'column', height: '297', width: '120', style: { "fontFamily": "Roboto,sans-serif", "outline": "1px solid #D8D8D8" }, marginBottom: 35 },
                title: { text: 'VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#6E7191', '#404256'],
                xAxis: { categories: ['Violations'], labels: { enabled: false }, title: { text: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: false },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: [{
                    name: 'Level 8',
                    data: [level8Total]
                }, {
                    name: 'Level 7',
                    data: [level7Total]
                }, {
                    name: 'Level 6',
                    data: [level6Total]
                }, {
                    name: 'Level 5',
                    data: [level5Total]
                }, {
                    name: 'Level 4',
                    data: [level4Total]
                }, {
                    name: 'Level 3',
                    data: [level3Total]
                }, {
                    name: 'Level 2',
                    data: [level2Total]
                }, {
                    name: 'Level 1',
                    data: [level1Total]
                }]
            };
        }
        if (type == "D") {
            this.violationDTD = {
                chart: { type: 'column', height: '297', width: '120', style: { "fontFamily": "Roboto,sans-serif", "outline": "1px solid #D8D8D8" }, marginBottom: 35 },
                title: { text: 'VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#6E7191', '#404256'],
                xAxis: { categories: ['Violations'], labels: { enabled: false }, title: { text: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: false },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: [{
                    name: 'Level 8',
                    data: [level8Total]
                }, {
                    name: 'Level 7',
                    data: [level7Total]
                }, {
                    name: 'Level 6',
                    data: [level6Total]
                }, {
                    name: 'Level 5',
                    data: [level5Total]
                }, {
                    name: 'Level 4',
                    data: [level4Total]
                }, {
                    name: 'Level 3',
                    data: [level3Total]
                }, {
                    name: 'Level 2',
                    data: [level2Total]
                }, {
                    name: 'Level 1',
                    data: [level1Total]
                }]
            };
        }
    }

    loadNoticesChart(results, type: String) {
        let self = this;
        let xAxis = [];
        for (var i in results) {
            if (xAxis.indexOf(results[i]._source.time_series_value) == -1)
                xAxis.push(results[i]._source.time_series_value)
        }

        let forecastData = [];
        let actualData = [];
        for (var item in results) {
            // let dataItem = [results[item]._source.infringement_level];
            // console.log(dataItem);
            forecastData.push(results[item]._source.forecast_notices);
            actualData.push(results[item]._source.actual_notices);
        }

        if (type == "Y") {
            self.noticesChartYTD = {
                chart: { type: 'spline', backgroundColor: '#494B62', width: '320', height: '215' },
                colors: ['#FFC31F', '#688CA5'],
                title: { text: '', align: 'left', style: { "color": "#fff" } },
                xAxis: { labels: { enabled: false }, tickWidth: 0 },
                yAxis: { title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0, tickLength: 0 },
                credits: { enabled: false },
                tooltip:{headerFormat: '',},
                plotOptions: { series: { marker: { enabled: true } } },
                series: [{ data: forecastData, showInLegend: false, name: "Forecast"  }, { data: actualData, showInLegend: false, name: "Actual"  }]
            };
        }

        if (type == "D") {
            self.noticesChartDTD = {
                chart: { type: 'spline', backgroundColor: '#494B62', width: '320', height: '215' },
                colors: ['#FFC31F', '#688CA5'],
                title: { text: '', align: 'left', style: { "color": "#fff" } },
                xAxis: { labels: { enabled: false }, tickWidth: 0 },
                yAxis: { title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0, tickLength: 0 },
                credits: { enabled: false },
                tooltip:{headerFormat: '',},
                plotOptions: { series: { marker: { enabled: true } } },
                series: [{ data: forecastData, showInLegend: false, name: "Forecast" }, { data: actualData, showInLegend: false, name: "Actual" }]
            };
        }

        if (type == "M") {
            self.noticesChartMTD = {
                chart: { type: 'spline', backgroundColor: '#494B62', width: '320', height: '215' },
                colors: ['#FFC31F', '#688CA5'],
                title: { text: '', align: 'left', style: { "color": "#fff" } },
                xAxis: { labels: { enabled: false }, tickWidth: 0 },
                yAxis: { title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0, tickLength: 0 },
                credits: { enabled: false },
                tooltip:{headerFormat: '',},
                plotOptions: { series: { marker: { enabled: true } } },
                series: [{ data: forecastData, showInLegend: false, name: "Forecast" }, { data: actualData, showInLegend: false, name: "Actual" }]
            };
        }

        if (type == "Q") {
            self.noticesChartQTD = {
                chart: { type: 'spline', backgroundColor: '#494B62', width: '320', height: '215' },
                colors: ['#FFC31F', '#688CA5'],
                title: { text: '', align: 'left', style: { "color": "#fff" } },
                xAxis: { labels: { enabled: false }, tickWidth: 0 },
                yAxis: { title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0, tickLength: 0 },
                credits: { enabled: false },
                tooltip:{headerFormat: '',},
                plotOptions: { series: { marker: { enabled: true } } },
                series: [{ data: forecastData, showInLegend: false, name: "Forecast" }, { data: actualData, showInLegend: false, name: "Actual" }]
            };
        }
    }

    loadTrialTotalUploads(results, type: String) {
        let xAxis = [];
        //Get the x axis Array
        for (var i in results) {
            if (xAxis.indexOf(results[i]._source.time_series_value) == -1)
                xAxis.push(results[i]._source.time_series_value)
        }
        let level8Data = new Array(xAxis.length).fill(0);
        let level1Data = new Array(xAxis.length).fill(0);
        let level2Data = new Array(xAxis.length).fill(0);
        let level3Data = new Array(xAxis.length).fill(0);
        let level4Data = new Array(xAxis.length).fill(0);
        let level5Data = new Array(xAxis.length).fill(0);
        let level6Data = new Array(xAxis.length).fill(0);
        let level7Data = new Array(xAxis.length).fill(0);
        //Now get the 
        for (var i in results) {
            for (var x in xAxis) {
                if (xAxis[x] == results[i]._source.time_series_value) {
                    if (results[i]._source.infringement_level_name == "Level 8") {
                        level8Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 1") {
                        level1Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 2") {
                        level2Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 3") {
                        level3Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 4") {
                        level4Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 5") {
                        level5Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 6") {
                        level6Data[x] = results[i]._source.total_notices;
                    }
                    if (results[i]._source.infringement_level_name == "Level 7") {
                        level7Data[x] = results[i]._source.total_notices;
                    }
                }
            }
        }
        var chartWidth = this.businessLine + (this.businessLine*0.14);
        //Set the yearly total upload chart
        if (type == "Y") {
            this.totalUploadsYTD = {
            condition: {
                maxWidth: 500
            },
                chart: { type: 'column', height: '297', width: chartWidth, style: { "fontFamily": "Roboto,sans-serif", "outline": "1px solid #D8D8D8" } },
                title: { text: 'NOTICE TOTAL UPLOAD EVENTS', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#6E7191', '#404256'],
                xAxis: { categories: xAxis, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: false },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: [{ name: 'Level 8', data: level8Data }, { name: 'Level 7', data: level7Data }, { name: 'Level 6', data: level6Data }, { name: 'Level 5', data: level5Data }, { name: 'Level 4', data: level4Data }, { name: 'Level 3', data: level3Data }, { name: 'Level 2', data: level2Data }, { name: 'Level 1', data: level1Data }]
            };
        }
        if (type == "Q") {
            this.totalUploadsQTD = {
                chart: { type: 'column', height: '297', width: chartWidth, style: { "fontFamily": "Roboto,sans-serif", "outline": "1px solid #D8D8D8" } },
                title: { text: 'NOTICE TOTAL UPLOAD EVENTS', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#6E7191', '#404256'],
                xAxis: { categories: xAxis, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: false },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: [{ name: 'Level 8', data: level8Data }, { name: 'Level 7', data: level7Data }, { name: 'Level 6', data: level6Data }, { name: 'Level 5', data: level5Data }, { name: 'Level 4', data: level4Data }, { name: 'Level 3', data: level3Data }, { name: 'Level 2', data: level2Data }, { name: 'Level 1', data: level1Data }]
            };
        }
        if (type == "M") {
            this.totalUploadsMTD = {
                chart: { type: 'column', height: '297', width: chartWidth, style: { "fontFamily": "Roboto,sans-serif", "outline": "1px solid #D8D8D8" } },
                title: { text: 'NOTICE TOTAL UPLOAD EVENTS', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#6E7191', '#404256'],
                xAxis: { categories: xAxis, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: false },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: [{ name: 'Level 8', data: level8Data }, { name: 'Level 7', data: level7Data }, { name: 'Level 6', data: level6Data }, { name: 'Level 5', data: level5Data }, { name: 'Level 4', data: level4Data }, { name: 'Level 3', data: level3Data }, { name: 'Level 2', data: level2Data }, { name: 'Level 1', data: level1Data }]
            };
        }
        if (type == "D") {
            this.totalUploadsDTD = {
                chart: { type: 'column', height: '297', width: chartWidth, style: { "fontFamily": "Roboto,sans-serif", "outline": "1px solid #D8D8D8" } },
                title: { text: 'NOTICE TOTAL UPLOAD EVENTS', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#6FCF00', '#ADE877', '#FEC000', '#F57910', '#F14061', '#BB1B2E', '#6E7191', '#404256'],
                xAxis: { categories: xAxis, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: false },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: [{ name: 'Level 8', data: level8Data }, { name: 'Level 7', data: level7Data }, { name: 'Level 6', data: level6Data }, { name: 'Level 5', data: level5Data }, { name: 'Level 4', data: level4Data }, { name: 'Level 3', data: level3Data }, { name: 'Level 2', data: level2Data }, { name: 'Level 1', data: level1Data }]
            };
        }
    }

    loadTotalNotices(results, type: String) {
        let self = this;
        console.log("Total notices: ", results);
        if (type == "Y") {
            self.ytdTotalNotices = results[0]._source;
        }
        if (type == "D") {
            self.dtdTotalNotices = results[0]._source;
        }
        if (type == "M") {
            self.mtdTotalNotices = results[0]._source;
        }
        if (type == "Q") {
            self.qtdTotalNotices = results[0]._source;
        }
    }

    loadContentOwner(results, type: String) {
        let self = this;
        if (type == "Y") {
            self.contentOwnerYTD = results;
        }
        if (type == "D") {
            self.contentOwnerDTD = results;
        }
        if (type == "Q") {
            self.contentOwnerQTD = results;
        }
        if (type == "M") {
            self.contentOwnerMTD = results;
        }
        for (var i in results) {

        }

    }
    loadServiceType(results: any, type: String) {
        let self = this;
        let eventArr = [];
        for (var i in results) {
            eventArr.push({
                name: results[i]._source.subscriber_service_used,
                data: [results[i]._source.total_notices],
                type: 'column',
                pointWidth: 100
            })
        }
        var chartWidth = (this.businessLine) - (this.businessLine*0.3)
        if (type == "Y") {
            this.methodViolationYTD = {
                chart: { type: 'column', height: '157.5', width: chartWidth, style: { fontFamily: 'Roboto,sans-serif' }, inverted: true },
                title: { text: 'METHOD OF VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#FE3E81', '#E1BCE7', '#BCBDBD', '#757575', '#FE5916', '#F8BACF', '#EA1C62', '#C2165B', '#9D18B2', '#7D0FA4'],
                xAxis: { categories: ['Service Type'], labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: true, itemStyle: { "fontSize": "11px", "fontWeight": "normal" }, symbolHeight: 15, borderWidth: 0, symbolRadius: 0, reversed: true, itemMarginTop: 5 },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: eventArr
            };
        }
        if (type == "D") {
            this.methodViolationDTD = {
                chart: { type: 'column', height: '157.5', width: chartWidth, style: { fontFamily: 'Roboto,sans-serif' }, inverted: true },
                title: { text: 'METHOD OF VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#FE3E81', '#E1BCE7', '#BCBDBD', '#757575', '#FE5916', '#F8BACF', '#EA1C62', '#C2165B', '#9D18B2', '#7D0FA4'],
                xAxis: { categories: ['Service Type'], labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: true, itemStyle: { "fontSize": "11px", "fontWeight": "normal" }, symbolHeight: 15, borderWidth: 0, symbolRadius: 0, reversed: true, itemMarginTop: 5 },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: eventArr
            };
        }
        if (type == "Q") {
            this.methodViolationQTD = {
                chart: { type: 'column', height: '157.5', width: chartWidth, style: { fontFamily: 'Roboto,sans-serif' }, inverted: true },
                title: { text: 'METHOD OF VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#FE3E81', '#E1BCE7', '#BCBDBD', '#757575', '#FE5916', '#F8BACF', '#EA1C62', '#C2165B', '#9D18B2', '#7D0FA4'],
                xAxis: { categories: ['Service Type'], labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: true, itemStyle: { "fontSize": "11px", "fontWeight": "normal" }, symbolHeight: 15, borderWidth: 0, symbolRadius: 0, reversed: true, itemMarginTop: 5 },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: eventArr
            };
        }
        if (type == "M") {
            this.methodViolationMTD = {
                chart: { type: 'column', height: '157.5', width: chartWidth, style: { fontFamily: 'Roboto,sans-serif' }, inverted: true },
                title: { text: 'METHOD OF VIOLATION', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
                colors: ['#FE3E81', '#E1BCE7', '#BCBDBD', '#757575', '#FE5916', '#F8BACF', '#EA1C62', '#C2165B', '#9D18B2', '#7D0FA4'],
                xAxis: { categories: ['Service Type'], labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                legend: { enabled: true, itemStyle: { "fontSize": "11px", "fontWeight": "normal" }, symbolHeight: 15, borderWidth: 0, symbolRadius: 0, reversed: true, itemMarginTop: 5 },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
                credits: { enabled: false },
                series: eventArr
            };
        }

        //Set static data for content type chart
        this.contentType = {
            chart: { type: 'column', height: '157.5', width: chartWidth, style: { fontFamily: 'Roboto,sans-serif' }, inverted: true },
            title: { text: 'CONTENT TYPE', align: 'left', style: { "fontSize": "15px", "color": "#000", "fontWeight": "bold" } },
            colors: ['#BCBDBD', '#757575', '#FE5916', '#F8BACF', '#EA1C62', '#C2165B'],
            xAxis: { categories: ['Content Type'], labels: { enabled: false }, title: { text: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
            yAxis: { min: 0, title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
            legend: { enabled: true, layout: 'vertical', align: 'left', verticalAlign: 'middle', itemStyle: { "fontSize": "11px", "fontWeight": "normal", lineHeight: "15px" }, symbolHeight: 15, borderWidth: 0, symbolRadius: 0, itemMarginTop: 5, y: 10, reversed: true, lineHeight: 15 },
            plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' }, series: { animation: false } },
            credits: { enabled: false },
            series: [{ name: 'Image', data: [9], pointWidth: 50 }, { name: 'Apps', data: [5], pointWidth: 50 }, { name: 'Games', data: [11], pointWidth: 50 }, { name: 'TV', data: [7], pointWidth: 50 }, { name: 'Music', data: [15], pointWidth: 50 }, { name: 'Movie', data: [21], pointWidth: 50 }]
        };
    }

    tabChanged(mode: any) {
        let self = this;
        if (mode.index == 0) { //Year Tab
            self.ytdSelected = true;
            self.qtdSelected = false;
            self.mtdSelected = false;
            self.dtdSelected = false;
        }
        if (mode.index == 1) { //Q Tab
            self.ytdSelected = false;
            self.qtdSelected = true;
            self.mtdSelected = false;
            self.dtdSelected = false;
        }
        if (mode.index == 2) { //Month Tab
            self.ytdSelected = false;
            self.qtdSelected = false;
            self.mtdSelected = true;
            self.dtdSelected = false;
        }
        if (mode.index == 3) { //Day Tab
            self.ytdSelected = false;
            self.qtdSelected = false;
            self.mtdSelected = false;
            self.dtdSelected = true;
        }
    }

    dashboardTigger() {
        this.searchEvent.emit();
    }
}