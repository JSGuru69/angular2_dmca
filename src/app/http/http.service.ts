import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { environment } from './../../environments/environment';
import { Cookie } from 'ng2-cookies';

const appConfig = require('../../../appConfig.js');
// console.log(appConfig);

@Injectable()

export class API {
    constructor(private http: Http) {
    }
    //apiPath: String = appConfig.baseUri;
    private environmentName = environment.name;
    //token: String = Cookie.get('token')
    // token:String = localStorage.getItem('token');
    //For regular http call

    fetchData(method, url, success, error, param ={} ) {
        const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : 'Bearer ' + Cookie.get('accessToken') })
        if(this.environmentName === 'dev')
            url = appConfig.devUri + url;
        if(this.environmentName === 'prod')
             url = appConfig.prodUri + url;

        if (method == 'get'){
            if(Object.keys(param).length>0)
                url= url+ param["text"];
            var api = this.http.get(url, { headers: headers })
        }
        else if (method == 'post')
            var api = this.http.post(url, param, { headers: headers })
        else if (method == 'put')
            var api = this.http.post(url, param, { headers: headers })
        else if (method == 'delete')
            var api = this.http.delete(url, { headers: headers })
        api.toPromise().then(success).catch(error)
    }

}