import { Component, AfterViewInit, Attribute, Output, EventEmitter } from '@angular/core';
import { InputFilter } from '../../services/query.service';
import { API } from '../../http/http.service';
//import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';

@Component({
    selector: 'ogc-dashboard',
    templateUrl: './ogc-dashboard.component.html',
    styleUrls: ['../../main.scss'],
    providers: [InputFilter, API]
})
export class OGCDashboardComponent implements AfterViewInit {
    private dashboard = require("assets/dashboard.png");
    dayLineOption: Object;
    businessLine: any;
    chartOne: any;
    chartTwo: any;
    chartThree: any;
    businessBar: any;
    totalWidth: any;
    dayBarOption: Object;
    dayBarWidth: any;
    dayBarStatus: any;

    noticesChartDTD: Object;
    mitigationChartDTD: Object;
    avgViolationChartDTD: Object;

    totalCount: number = 0;
    loading: boolean = false;

    totalNoticeWidth: any;

    notFound: Boolean = false;
    responseLength: any;

    dtdTotalNotices: any = {};

    constructor(private httpservice: API) { //private _service: NotificationsService,) {
    };


    ngAfterViewInit() {
        let self = this;
        self.loading = true;
        var elem = document.querySelector('.search-exp').clientWidth;
        self.totalWidth = elem;
        console.log('elem', elem);
        if (elem > 700) {
            self.businessLine = (elem / 2.5) - 30;
            self.chartOne = elem / 2.5;
            self.totalNoticeWidth = (elem - self.chartOne) - 30;
            console.log('businessLine', self.businessLine);
            self.businessBar = ((elem - self.businessLine) / 2) - 30;
            self.chartTwo = (elem - self.businessLine) / 2.5;
            self.chartThree = self.chartTwo - 45;
            console.log('businessBar', self.businessBar);
            self.dayBarWidth = self.chartTwo / 3.5;
            self.dayBarStatus = self.chartTwo - self.dayBarWidth;
        }
        else {
            self.businessLine = elem - 30;
            self.chartOne = elem;
            self.totalNoticeWidth = elem;
            console.log('businessLine', self.businessLine);
            self.businessBar = elem - 30;
            self.chartTwo = elem;
            self.chartThree = elem;
            console.log('businessBar', self.businessBar);
            self.dayBarWidth = self.businessBar / 3;
            self.dayBarStatus = self.businessBar - self.dayBarWidth;
        }
        self.ogcDashboardAPICalls();
    }

    ogcDashboardAPICalls() {
        let self = this;
        let url = "ogcbusiness"
        self.httpservice.fetchData('get', url, (data) => {
            var response = JSON.parse(data._body);

            var totalNoticesDTD = JSON.parse(response.totalNoticesDTD);
            var totalNoticesChartDTD = JSON.parse(response.totalNoticesChartDTD);
            var chartsDTD = JSON.parse(response.chartsDTD);

            console.log(totalNoticesDTD);
            console.log(totalNoticesChartDTD);
            console.log(chartsDTD);

            if (totalNoticesDTD.hasOwnProperty("hits")) self.loadTotalNotices(totalNoticesDTD.hits.hits, "D");
            if (totalNoticesChartDTD.hasOwnProperty("hits")) self.loadNoticesChart(totalNoticesChartDTD.hits.hits, "D");
            if (chartsDTD.hasOwnProperty("hits")) self.loadCharts(chartsDTD.hits.hits, "D");
            self.loading = false;
        }, (error) => { console.log(error) })
    }

    loadTotalNotices(results, type: String) {
        let self = this;
        console.log("Total notices: ", results);
        // if (type == "Y") {
        //     self.ytdTotalNotices = results[0]._source;
        // }
        if (type == "D") {
            self.dtdTotalNotices = results[0]._source;
        }
    }

    loadNoticesChart(results, type: String) {
        let self = this;
        let xAxis = [];
        for (var i in results) {
            if (xAxis.indexOf(results[i]._source.time_series_value) == -1)
                xAxis.push(results[i]._source.time_series_value)
        }

        let forecastData = [];
        let actualData = [];
        for (var item in results) {
            // let dataItem = [results[item]._source.infringement_level];
            // console.log(dataItem);
            forecastData.push(results[item]._source.forecast_notices);
            actualData.push(results[item]._source.actual_notices);
        }

        self.noticesChartDTD = {
            chart: { type: 'spline', backgroundColor: '#494B62', width: '320', height: '215' },
            colors: ['#FFC31F', '#688CA5'],
            title: { text: '', align: 'left', style: { "color": "#fff" } },
            xAxis: { labels: { enabled: false }, tickWidth: 0 },
            yAxis: { title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0, tickLength: 0 },
            credits: { enabled: false },
            tooltip: { headerFormat: '', },
            plotOptions: { series: { marker: { enabled: true } } },
            series: [{ data: forecastData, showInLegend: false, name: "Forecast" }, { data: actualData, showInLegend: false, name: "Actual" }]
        };
    }

    loadCharts(results, type: String) {
        let self = this;
        let xAxisMitigation = [];
        let xAxisAvg = [];
        for (var i in results) {
            if (xAxisMitigation.indexOf(results[i]._source.infringement_level_name) == -1) {
                if (results[i]._source.infringement_level_name != null)
                    xAxisMitigation.push(results[i]._source.infringement_level_name)
            }
            if (xAxisAvg.indexOf(results[i]._source.infringement_level_value) == -1) {
                if (results[i]._source.infringement_level_value != null)
                    xAxisAvg.push(results[i]._source.infringement_level_value)
            }
        }

        let nonRepeat = [];
        let repeat = [];
        let avgViolation = [];
        for (var item in results) {
            if (results[item]._source.infringement_level_value != null || results[item]._source.infringement_level_name != null) {
                nonRepeat.push(results[item]._source.non_repeat_pct);
                repeat.push(results[item]._source.repeat_pct);
                avgViolation.push(results[item]._source.avg_notices);
            }
        }
        var elem = document.querySelector('.search-exp').clientWidth;
        var width = elem - (elem * 0.02);

        self.mitigationChartDTD = {
            chart: { type: 'column', backgroundColor: '#fff', width: width, height: '248' },
            colors: ['#CE6A72', '#B31A26'],
            title: { text: 'MITIGATION SUCCESS', align: 'left', style: { "color": "#000", "fontSize": "18px" } },
            xAxis: { categories: xAxisMitigation, reversed: true, tickWidth: 0, lineWidth: 0 },
            yAxis: {
                title: { text: null }, labels: { enabled: false }, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: '#fff', minorTickLength: 0,
                tickLength: 0,
                stackLabels: {enabled: true,style: {fontWeight: 'bold',color: 'gray'} }
            },
            legend: { layout: 'vertical', align: 'left', x: -10, verticalAlign: 'top', y: 40, margin: 30, symbolRadius: 0, reversed: true, borderWidth: 0, symbolHeight: 15, itemMarginTop: 6, itemStyle: { "fontSize": "12px", "fontWeight": "normal" } },
            credits: { enabled: false },
            tooltip: { headerFormat: '', },
            plotOptions: {
                column: { stacking: 'percent', borderWidth: 0 }
            },
            series: [{ name: 'Non-Repeat', data: nonRepeat, showInLegend: true }, { name: 'Repeat', data: repeat, showInLegend: true }]

        };
        var wid = this.chartThree + 90;
        self.avgViolationChartDTD = {
            chart: { type: 'bar', inverted: true, height: '344', width: wid, backgroundColor: '#494B62' },
            colors: ['#FFC31F'],
            title: { text: 'AVERAGE NUMBER OF VIOLATIONS/LEVEL', align:'center',style: { "color": "#fff", "fontSize": "15px" } },
            xAxis: { categories: xAxisAvg, tickWidth: 0, labels: { style: { color: '#fff' } } },
            yAxis: { labels: { enabled: false }, title: { text: null } },
            plotOptions: {series: {borderColor: '#FFC31F', pointWidth: 26}},
            credits: { enabled: false },
            series: [{ name: "Average Violations", data: avgViolation, showInLegend: false }]
        }
    }

}