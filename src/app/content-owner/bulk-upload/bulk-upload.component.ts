import { Component, OnInit } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { SharedStoreService } from './../../shared.store';
import { API } from '../../http/http.service';
import {Message, GrowlModule} from 'primeng/primeng';
//import { NotificationsService,SimpleNotificationsModule } from 'angular2-notifications';

@Component({
    selector: 'dmca-bulk-upload',
    templateUrl: './bulk-upload.component.html',
    styleUrls: ['../../main.scss'],
    providers: [SharedStoreService, API]
})
export class BulkUploadComponent implements OnInit {

    //private logo = require("assets/pwc-logo.png");
    msgs: Message[] = [];
    submitDate:String;
    searchExp: Boolean = true;
    modelVisible: Boolean = false;
    bulkSubmit: Boolean = false;
    confirm: Boolean = true;
    totalCount: any;
    validCount: any;
    invalidCount: any;
    loading: boolean = false;
    public options = {
        timeOut: 5000,
        lastOnBottom: true,
        clickToClose: true,
        maxLength: 0,
        maxStack: 7,
        showProgressBar: false,
        pauseOnHover: true,
        preventDuplicates: false,
        preventLastDuplicates: 'visible',
        rtl: false,
        animate: 'scale',
        position: ['right', 'top']
    };

    constructor(private httpservice: API, private store: SharedStoreService) { //, private _service: NotificationsService) {

    };
    onSearchExp() {
        this.searchExp = !this.searchExp;
    }
    showDeclaration() {
        var self = this;
        self.modelVisible = !self.modelVisible;
    }
    cloudUpload() {
        var self = this;
        
        //let config = new MdSnackBarConfig();
        //config.duration = this.autoHide;
        console.log("Valid: ", self.store.getValidCount(), "Invalid:", self.store.getInvalidCount(), "total: ", self.store.getTotalCount());
        console.log(self.store.getValidList());
        self.modelVisible = false;
        self.loading = true;
        self.httpservice.fetchData('post', 'upload', (data) => {
            console.log(data);
            self.modelVisible = !self.modelVisible;
            // alert("Violations successfully submitted");
            this.msgs = [];
            this.msgs.push({severity:'success', summary:'Success', detail:'Violations successfully submitted'});
            //this._service.success("Success", "Violations submitted");
            self.confirm = false;
            self.bulkSubmit = true;
            self.loading = false;
            self.modelVisible = false;
            //self.toastr.success("Violations submitted!")
        }, (error) => {
            console.log(error);
            self.modelVisible = !self.modelVisible;
            // alert("Error in submitting violations. Please try again later");
            this.msgs = [];
            this.msgs.push({severity:'error', summary:'Error', detail:'Error in submitting violation. Please try again later'});
            //this._service.error("Error", "Error in submitting violations. Please try again later!");
        }, self.store.getValidList());
    }

    ngOnInit() {
        var self = this;
        self.validCount = self.store.getValidCount();
        self.invalidCount = self.store.getInvalidCount();
        self.totalCount = self.store.getTotalCount();
        self.submitDate = new Date().toLocaleDateString('en-US', {timeZone: 'UTC'});
    }
}