import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { SharedStoreService } from './../../shared.store'
import { environment } from './../../../environments/environment';
//import * as _ from "lodash";
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Cookie } from 'ng2-cookies';

const appConfig = require('../../../../appConfig.js');

@Component({
    selector: 'dmca-submit',
    templateUrl: './submission.component.html',
    styleUrls: ['../../main.scss'],
    providers: [SharedStoreService]
})
export class SubmissionComponent {
    private content = require("assets/Content.png");
    private manual = require("assets/manual.svg");
    private handshake = require("assets/handshake.svg");
    private logo = require("assets/pwc-logo.png");
    filesToUpload: File;
    //public uploader:FileUploader = new FileUploader({url: URL});
    //public hasBaseDropZoneOver:boolean = false;
    //public hasAnotherDropZoneOver:boolean = false;
    violationList: any;
    validList: any;
    loading: boolean = false;
    //validCount:any;
    //invalidCount:any;
    //totalCount:any;
    iframeWidth: Number = (window.innerWidth) - 50;
    private environmentName = environment.name;
    private mail_to: string = "mailto:" + appConfig.infringementNotifyEmailId + "?subject=Notice%20of%20Claimed%20Infringement%20-%20Case%20ID%20&body=-----BEGIN%20PGP%20SIGNED%20MESSAGE-----%0AHash%3A%20SHA1%0ADear%20ISP%20Provider%0A%0AWe%20have%20received%20information%20that%20an%20subscriber%20who%20owns%20your%20IP%20address%20has%20downloaded%20the%20copyrighted%20material.%0A%0AThe%20distribution%20of%20unauthorized%20copies%20of%20copyrighted%20content%20constitutes%20copyright%20infringement%20under%20the%20Copyright%20Act%2C%20Title%2017%20United%20States%20Code%20Section%20106(3).%20This%20conduct%20may%20also%20violate%20the%20laws%20of%20other%20countries%2C%20international%20law%2C%20and%2For%20treaty%20obligations.%0A%0ASince%20you%20own%20this%20IP%20address%20below%2C%20we%20request%20that%20you%20immediately%20do%20the%20following%3A%0A%0A1)%20Contact%20the%20subscriber%20who%20has%20engaged%20in%20the%20conduct%20described%20above%20and%20take%20steps%20to%20prevent%20the%20subscriber%20from%20further%20downloading%20or%20uploading%20the%20copyright%20content%20without%20authorization.%0A%0A2)%20Take%20appropriate%20action%20against%20the%20account%20holder%20under%20your%20Abuse%20Policy%2FTerms%20of%20Service%20Agreement.%0A%0AWe%20own%20the%20exclusive%20rights%20in%20the%20copyrighted%20material%20at%20issue%20in%20this%20notice%2C%20we%20hereby%20state%20that%20we%20have%20a%20good%20faith%20belief%20that%20use%20of%20the%20material%20in%20the%20manner%20complained%20was%20not%20authorized%2C%20its%20respective%20agents%2C%20or%20the%20law.%0A%0AAlso%2C%20we%20hereby%20state%2C%20under%20penalty%20of%20perjury%2C%20that%20we%20are%20authorized%20to%20act%20on%20behalf%20of%20the%20owner%20of%20the%20exclusive%20rights%20being%20infringed%20as%20set%20forth%20in%20this%20notification.%0A%0AWe%20appreciate%20your%20assistance%20and%20thank%20you%20for%20your%20cooperation%20in%20this%20matter.%20Your%20prompt%20response%20is%20requested.%0A%0AThanks%0AAnti-piracy%20team%0A%0A%0A---------------%20Infringement%20Details%20----------------------------------%0ATitle%3A%20-content-%0ATimestamp%3A%20-timestamp-Z%0AIP%20Address%3A%20-ip-%0APort%3A%20-port-%0AType%3A%20BitTorrent%0AHash%3A%20-hash-%0AFilename%3A%20-filename-%0AFilesize%3A%201420%20MB%0A-----------------------------------------------------------------------------%0A";
    options: Object;
    constructor(private router: Router, private store: SharedStoreService, private http: Http) {
        this.validList = [];
        this.loadUploadsChart();
    };
    loadUploadsChart() {
        let self = this;
        self.loading = true;
        const headers = new Headers({ 'Content-Type': 'application/json', 'Authorization' : 'Bearer ' + Cookie.get('accessToken') });
        let url = '';
        if (self.environmentName === 'dev')
            url = appConfig.devUri + 'notificationsupload';
        if (self.environmentName === 'prod')
            url = appConfig.prodUri + 'notificationsupload';
        self.http.get(url, { headers: headers }).toPromise().then((data) => {
            let results = data.json().hits.hits;
            console.log("Uploads: ", results)
            let emailData = new Array(results.length).fill(0);
            let bulkData = new Array(results.length).fill(0);
            let webData = new Array(results.length).fill(0);
            let xAxis = [];

            for (var i in results) {
                if (xAxis.indexOf(results[i]._source.month) == -1) {
                    var xdata = results[i]._source.month + ' ' + results[i]._source.year;
                    xAxis.push(xdata)
                }
            }

            for (var i in results) {
                var xdata = results[i]._source.month + ' ' + results[i]._source.year;
                for (var x in xAxis) {
                    if (xAxis[x] == xdata) {
                        if (results[i]._source.Email != undefined) emailData[x] = results[i]._source.Email;
                        if (results[i]._source["UI - Bulk Upload"] != undefined) bulkData[x] = results[i]._source["UI - Bulk Upload"];
                        if (results[i]._source["UI - Web Form"] != undefined) webData[x] = results[i]._source["UI - Web Form"];
                    }
                }
            }

            this.options = {
                chart: { type: 'column', height: '250', width: '980', style: { fontFamily: 'Roboto,sans-serif' } },
                title: { text: 'VIOLATION UPLOAD EVENTS', align: 'left', style: { "fontSize": "18px" } },
                colors: ['#FEC000', '#F14061', '#BA1F30'],
                xAxis: { categories: xAxis, lineWidth: 0, minorGridLineWidth: 0, lineColor: 'transparent', minorTickLength: 0, tickLength: 0 },
                yAxis: { min: 0, lineWidth: 0, minorGridLineWidth: 0, gridLineColor: 'transparent', minorTickLength: 0, tickLength: 0, labels: { enabled: false }, title: { text: null } },
                legend: { layout: 'vertical', align: 'left', x: -10, verticalAlign: 'top', y: 40, margin: 30, symbolRadius: 0, reversed: true, borderWidth: 0, symbolHeight: 15, itemMarginTop: 6, itemStyle: { "fontSize": "11px", "fontWeight": "normal" } },
                plotOptions: { animation: false, colorByPoint: true, column: { stacking: 'normal' } },
                credits: { enabled: false },
                series: [{
                    name: 'Email Submission',
                    data: emailData
                }, {
                    name: 'Bulk Upload',
                    data: bulkData
                }, {
                    name: 'Manual Submission',
                    data: webData
                }]
            };
            self.loading = false;
        }).catch((error) => {
            console.log(error); self.loading = false;
        });

    }
    onSyncClick() {
        this.router.navigate(['bulkupload']);
    }
    onManualSub() {
        this.router.navigate(['manual-submission']);
    }
    mailViolation() {
        window.open(this.mail_to, '_blank');
    }
    mlClick() {
        window.open('http://pwc-pirates-ml.appspot-preview.com/dmcaai', '_blank');
    }

    fileChangeEvent(fileInput: any) {
        var self = this;
        var URL;
        console.log("Selected files: ", fileInput.target.files[0]);
        self.filesToUpload = <File>fileInput.target.files[0];
        self.loading = true;
        if (self.environmentName === 'dev')
            URL = appConfig.devUri + 'validation';
        if (self.environmentName === 'prod')
            URL = appConfig.prodUri + 'validation';
        self.makeFileRequest(URL, self.filesToUpload).then((result) => {
            console.log("Returned:", result);
            self.violationList = result;
            self.extractValidViolations();
            self.router.navigate(['bulkupload']);
            self.loading = false;
        }, (error) => {
            console.error(error);
        });
    }

    makeFileRequest(url: string, file: File) {
        var self = this;
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'json';
            formData.append("file", file, file.name);
            console.log("Formdata: ", formData);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(xhr.response);
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Authorization", "Bearer " + Cookie.get('accessToken'));
            xhr.send(formData);
        });
    }

    extractValidViolations() {
        var self = this;
/*
        for (let item of self.violationList) {
            console.log(item);
            if (item.validity === 1) {
                self.validList.push(item);
            }
        }
*/       
        var apiPayload = {
                "node_host" : self.violationList.node_host,
                "file_path" : self.violationList.file_path
        }

        self.validList.push(apiPayload);
        self.store.setValidList(self.validList);
        self.store.setValidCount(self.violationList.validCount);
        self.store.setTotalCount(self.violationList.totalCount);
        self.store.setInvalidCount(self.violationList.invalidCount);
/*
        self.store.setValidList(self.validList);
        self.store.setValidCount(self.validList.length);
        self.store.setTotalCount(self.violationList.length);
        self.store.setInvalidCount(self.violationList.length - self.validList.length);
*/
        console.log("Valid: ", self.store.getValidCount(), "Invalid:", self.store.getInvalidCount(), "total: ", self.store.getTotalCount());
    }
}
