import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { API } from '../../http/http.service';
import {Message, GrowlModule} from 'primeng/primeng';

@Component({
    selector: 'manual-submission',
    templateUrl: './manual-submission.html',
    styleUrls: ['../../main.scss'],
    providers: [API]
})

export class ManualSubmissionComponent implements OnInit {
    public date: Date = new Date();
    public mytime: Date = new Date();
    private body: any;
    loading: boolean = false;
    owner: any =null;
    caseId: any = 0;
    serviceArray: any;
    ownerArray: any;
    ownerEmailArray: any;
    titleArray: any;
    titleFile: any;
    hashRanValue: any = "";
    portNumber: any = 0;
    splittedData: any;
    modelVisible: Boolean = false;
    formattedDate: any;
    public options = {
        timeOut: 5000,
        lastOnBottom: true,
        clickToClose: true,
        maxLength: 0,
        maxStack: 7,
        showProgressBar: false,
        pauseOnHover: true,
        preventDuplicates: false,
        preventLastDuplicates: 'visible',
        rtl: false,
        animate: 'scale',
        position: ['right', 'top']
    };
    msgs: Message[] = [];

    showTime() {
        var self = this;
        self.modelVisible = !self.modelVisible;
        console.log('working!');
    }
    timeApply() {
        var self = this;
        // self.formatedDate = self.date.toString();
        // console.log('formatedDate', self.formatedDate);
        var splitDate = self.date.toString().split(" ");
        var splitTime = self.mytime.toString().split(" ");
        console.log('time', self.mytime);
        self.formattedDate = splitDate[0] + " " + splitDate[1] + " " + splitDate[2] + " " + splitTime[4] + " UTC " + splitDate[3];
        console.log(self.formattedDate);
        self.modelVisible = false;
    }
    constructor(private httpservice: API, private router: Router, private _formBuilder: FormBuilder){
        this.caseId = Math.floor(10000000 + Math.random() * 90000000);
        this.portNumber = Math.floor(1000 + Math.random() * 9000);

        var possible = "abcdefghijklmnopqrstuvwxyz0123456789";        
        for( var i=0; i < 32; i++ ){
            this.hashRanValue += possible.charAt(Math.floor(Math.random() * possible.length));
        }
// Service Used autopopulation
        var serviceArray = ['Bit Torrent', 'Babelgum', 'Revision3', 'Gnutella', 'Kazaa', 'Joost', 'Jaman', 'BitTorrent', 'LiveStation', 'Mashboxx'];
            this.serviceArray = serviceArray[Math.floor(Math.random()*9)];
// owner and owneremail autopopulation
        var ownerArray = ['HBO Ic.', 'Showtime', 'ABC', 'Netflix', 'Columbia Pictures', 'Pixar', 'Warner Bros.', 'NBC Universal', 'Disney Inc.', 'BMG', 'Interscope', 'Universal Music Group', 'Sony Music', 'EMI Music', 'Warner Music Group'];
        var ownerEmailArray = ['hbo', 'showtime', 'abc', 'netflix', 'columbiapictures', 'pixar', 'warner', 'nbcuniversal', 'disney', 'bmg', 'interscope', 'universalmusic', 'sonymusic', 'emimusic', 'warnermusic'];
        var ranArray15 = Math.floor(Math.random() * 15);
           this.ownerArray = ownerArray[ranArray15];
           this.ownerEmailArray = "contact@"+ownerEmailArray[ranArray15]+".com";
// title and title filename autopopulation
        var titleArray = ['Game of Thrones S1:E1', 'Homeland S1:E1', 'Grey\'s Anatomy S1:E1', 'House of Cards S1:E1', 'The Bounty Hunter', 'Toy Story', 'Miami Vice', 'Oh My My', 'Tubular Bells', 'News of the World'];
        var titleFile = ['Game of Thrones S1:E1.avi', 'Homeland S1:E1.mpg', 'Grey\'s Anatomy S1:E1.mov', 'House of Cards S1:E1.qt', 'The Bounty Hunter.mp4', 'Toy Story.amv', 'Miami Vice.asf', 'Oh My My.wma', 'Tubular Bells.wav', 'News of the World.mp3'];
        var ranArray10 = Math.floor(Math.random() * 10);
           this.titleArray = titleArray[ranArray10];
           this.titleFile = titleFile[ranArray10];
    } //, private _service: NotificationsService) { }
    manualSubmission: FormGroup;
    ngOnInit() {
        var emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var self = this;
        this.manualSubmission = this._formBuilder.group({
            case_id: [this.caseId, [Validators.required]],
            owner_entity_name: [this.ownerArray, [Validators.required]],
            owner_email: [this.ownerEmailArray, [Validators.required, Validators.pattern(emailPattern)]],
            infringed_content: [this.titleArray, [Validators.required]],
            timestamp_infringed: ['', [Validators.required]],
            subscriber_ip: ['', [Validators.required]],
            hashvalue: [this.hashRanValue, [Validators.required]],
            infringed_filename: [this.titleFile, [Validators.required]],
            digital_signature: [this.ownerArray, [Validators.required]],
            accuracy_flag: ['', [Validators.required]],
            good_faith_flag: ['', [Validators.required]],
            provider_entity_name: ['ISP', [Validators.required]],
            provider_email: ['dmca@isp.net', [Validators.required]],
            subscriber_port: [this.portNumber, []],
            service_used: [this.serviceArray, []]
        })

    }

    callApi() {
        var self = this;
        console.log("Values:", self.manualSubmission.value);
        var nowTime = new Date().toString();
        var split = nowTime.split(" ");
        var formattedDate = split[0]+" "+split[1]+" "+split[2]+" "+split[4]+" UTC "+split[3];
        self.loading = true; 
        self.body=
           {
            "case_id": self.manualSubmission.value.case_id.toString(),
            "owner_entity_name": self.manualSubmission.value.owner_entity_name,
            "owner_email": self.manualSubmission.value.owner_email,
            "digital_signature": self.manualSubmission.value.digital_signature,
            "good_faith_flag": "YES",
            "accuracy_flag": "YES",
            "provider_entity_name": self.manualSubmission.value.provider_entity_name,
            "provider_email": self.manualSubmission.value.provider_email,
            "timestamp_received": formattedDate,
            "subscriber_ip": self.manualSubmission.value.subscriber_ip,
            "subscriber_port": self.manualSubmission.value.subscriber_port,
            "service_used": self.manualSubmission.value.service_used,
            "timestamp_infringed": self.manualSubmission.value.timestamp_infringed,
            "infringed_content": self.manualSubmission.value.infringed_content,
            "infringed_filename": self.manualSubmission.value.infringed_filename,
            "hashvalue": self.manualSubmission.value.hashvalue
           };
            console.log("Now time: ",self.body);
        self.httpservice.fetchData('post', 'manual' , (data) => {
            console.log(data);
            self.loading = false;
            // alert("Violations successfully submitted");
            this.msgs = [];
            this.msgs.push({severity:'success', summary:'Success', detail:'Violations successfully submitted'});
            //this._service.success("Success", "Violation submitted");
            console.log(this.msgs);
        }, (error) => {
            console.log(error);
            self.loading = false;
            // alert("Error in submitting violation. Please try again later");
            this.msgs = [];
            this.msgs.push({severity:'error', summary:'Error', detail:'Error in submitting violation. Please try again later'});
            // this._service.error("Error", "Error in submitting violation. Please try again later!");
            console.log(this.msgs);
            
        }, self.body);

    }

}