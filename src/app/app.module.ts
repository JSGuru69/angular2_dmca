import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";
import { ChartModule } from 'angular2-highcharts';
//import { SimpleNotificationsModule } from 'angular2-notifications';
import { DmcaRouting } from './app.routing';
import { DatepickerModule } from 'ng2-bootstrap/datepicker';
import { TimepickerModule } from 'ng2-bootstrap/timepicker';
import { InputTextModule, GrowlModule } from 'primeng/primeng';

import { AppComponent } from './app.component';
import { LoginComponent } from "./shared/login/login.component";
import { LandingComponent } from './landing-page/landing.component';
import { TeamDashboardComponent } from "./dcma-team/dashboard/team-dashboard.component";
import { OGCDashboardComponent } from "./ogc-business/ogc-dashboard/ogc-dashboard.component";
import { SubmissionComponent } from "./content-owner/submission/submission.component";
import { ManageComponent } from "./dmca-admin/manage/manage.component";
import { APIsComponent } from "./dmca-admin/apis/apis.component";
import { CreateRuleComponent } from "./dmca-admin/create-rule/create-rule.component";
import { CreateAPIComponent } from "./dmca-admin/create-api/create-api.component";
import { ManageSideNavComponent } from "./dmca-admin/manage-sidenav/manage-sidenav.component";
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { BulkUploadComponent } from './content-owner/bulk-upload/bulk-upload.component';
import { ManualSubmissionComponent } from './content-owner/manual-submission/manual-submission.component';
import { ViolationDetails } from './dcma-team/dashboard/violation-details/violation-details.component';
import { SideNavigation } from './shared/sidenav/sidenav';
import { InputFilter } from './services/query.service';
import { LoginPageComponent } from './login-page/login-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LandingComponent,
    TeamDashboardComponent,
    OGCDashboardComponent,
    SubmissionComponent,
    ManageComponent,
    APIsComponent,
    CreateRuleComponent,
    CreateAPIComponent,
    ManageSideNavComponent,
    HeaderComponent,
    FooterComponent,
    SideNavigation,
    BulkUploadComponent,
    ManualSubmissionComponent,
    ViolationDetails,
    InputFilter,
    LoginPageComponent
  ],
  imports: [
    BrowserModule,
    InputTextModule,
    GrowlModule,
    ChartModule,
    FormsModule,
    HttpModule,
    DmcaRouting,
    ReactiveFormsModule,
    //SimpleNotificationsModule.forRoot(),
    MaterialModule.forRoot(),
    FlexLayoutModule,
    DatepickerModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  private heading: string = "";
}
