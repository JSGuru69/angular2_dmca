import { Component } from '@angular/core';
import { environment } from './../../environments/environment';

const appConfig = require('../../../appConfig.js');

@Component({
    selector: 'dmca-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['../main.scss'],
})
export class LandingComponent {
    //private logo = require("assets/pwc-logo.png");
    private environmentName = environment.name;

    constructor() {};

    subscriberGo() {
        //window.location.href='http://portal.dmcainfringement.com:4200/subscriber/ISP-R-0001';
        if (this.environmentName === 'dev')
          window.open(appConfig.devDNSName + ':8080/subscriber/ISP-R-0010','_blank');
        else 
          window.open(appConfig.nodeDNSName + ':8080/subscriber/ISP-R-0010','_blank');
    }

     ogcClick() {
         window.open('http://powerbi.dmcainfringement.com/Dashboard/Report?reportId=e7b1e1b5-f4a6-4d51-b323-55270cf8d475', '_blank');
        //window.location.href='http://powerbi.dmcainfringement.com/Dashboard/Report?reportId=e7b1e1b5-f4a6-4d51-b323-55270cf8d475';
    }
}