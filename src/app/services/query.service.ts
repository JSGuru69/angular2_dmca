import { Pipe, PipeTransform } from '@angular/core'
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { environment } from './../../environments/environment';

const appConfig = require('../../../appConfig.js');

@Pipe({
    name: 'search',
     //pure: false
})
export class InputFilter implements PipeTransform {
    // constructor(private http: Http) {
    // }
    // private environmentName = environment.name;
    transform(value, args) {
        if (!args) {
            return value;
           
        } else if (value) {
            // console.log("call API here?");
            // var url = '/autocomplete/'
            // if(this.environmentName === 'dev')
            //     url = appConfig.devUri + url + args;
            // if(this.environmentName === 'prod')
            //      url = appConfig.prodUri + url + args;
            // const headers = new Headers({ 'Content-Type': 'application/json'});
            // this.http.get(url, { headers: headers }

            return value.filter(item => {
                console.log("filter",item);
                for (let key in item) {
                    if ((typeof item[key] === 'string' || item[key] instanceof String) &&
                        (item[key].toUpperCase().indexOf(args.toUpperCase()) !== -1)) {
                        return true
                    }
                }
            })
        }
    }
}