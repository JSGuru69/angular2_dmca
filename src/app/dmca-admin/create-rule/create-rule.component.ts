import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { SharedStoreService } from './../../shared.store'
import { environment } from './../../../environments/environment';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {Location} from '@angular/common';

@Component({
    selector: 'create-rule',
    templateUrl: './create-rule.component.html',
    styleUrls: ['../../main.scss'],
    providers: [SharedStoreService]
})
export class CreateRuleComponent {
    private content = require("assets/Content.png");
    private manual = require("assets/manual.svg");
    private handshake = require("assets/handshake.svg");
    private logo = require("assets/pwc-logo.png");

    showRules: false;
    showAPIs: false;
    showInfra: false;

    expandRulesMgmt() {
	  	if(this.showRules) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandAPIs() {
	  	if(this.showAPIs) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	infrastructureGo() {
        window.open('https://portal.azure.com','_blank');
    }

}
