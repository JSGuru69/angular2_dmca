import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { SharedStoreService } from './../../shared.store'
import { environment } from './../../../environments/environment';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {Location} from '@angular/common';

@Component({
    selector: 'dmca-admin',
    templateUrl: './manage.component.html',
    styleUrls: ['../../main.scss'],
    providers: [SharedStoreService]
})
export class ManageComponent {
    private content = require("assets/Content.png");
    private manual = require("assets/manual.svg");
    private handshake = require("assets/handshake.svg");
    private logo = require("assets/pwc-logo.png");

    showRules: false;
    showAPIs: false;
    showInfra: false;
    showRule: false;
    showRule2: false;
    showRule3: false;
    showRule4: false;
    showRule5: false;
    showRule6: false;
    showRule7: false;
    showRule8: false;
    showRule9: false;
    showRule10: false;
    showRule11: false;
    showRule12: false;
    showRule13: false;
    showRule14: false;
    showRule15: false;
    showRule16: false;
    showRule17: false;
    showRule18: false;
    showRule19: false;
    showRule20: false;
    showRule21: false;
    showRule22: false;

    expandRulesMgmt() {
	  	if(this.showRules) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandAPIs() {
	  	if(this.showAPIs) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	infrastructureGo() {
        window.open('https://portal.azure.com','_blank');
    }

	expandRule() {
	  	if(this.showRule) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule2() {
	  	if(this.showRule2) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule3() {
	  	if(this.showRule3) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule4() {
	  	if(this.showRule4) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule5() {
	  	if(this.showRule5) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule6() {
	  	if(this.showRule6) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule7() {
	  	if(this.showRule7) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule8() {
	  	if(this.showRule8) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule9() {
	  	if(this.showRule9) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule10() {
	  	if(this.showRule10) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule11() {
	  	if(this.showRule11) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule12() {
	  	if(this.showRule12) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule13() {
	  	if(this.showRule13) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule14() {
	  	if(this.showRule14) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule15() {
	  	if(this.showRule15) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule16() {
	  	if(this.showRule16) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule17() {
	  	if(this.showRule17) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule18() {
	  	if(this.showRule18) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule19() {
	  	if(this.showRule19) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule20() {
	  	if(this.showRule20) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule21() {
	  	if(this.showRule21) {
	      return "block";
	    } else {
	      return "";
	    }
	};
	expandRule22() {
	  	if(this.showRule22) {
	      return "block";
	    } else {
	      return "";
	    }
	};
}
