import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import { SharedStoreService } from './../../shared.store'
import { environment } from './../../../environments/environment';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import {Location} from '@angular/common';

@Component({
    selector: 'dmca-admin',
    templateUrl: './apis.component.html',
    styleUrls: ['../../main.scss'],
    providers: [SharedStoreService]
})
export class APIsComponent {
    private content = require("assets/Content.png");
    private manual = require("assets/manual.svg");
    private handshake = require("assets/handshake.svg");
    private logo = require("assets/pwc-logo.png");

    showRules: false;
    showAPIs: false;
    showInfra: false;
    showRule: false;
    showRule2: false;
    showRule3: false;
    showRule4: false;
    showRule5: false;
    showRule6: false;
    showRule7: false;
    showRule8: false;
    showRule9: false;
    showRule10: false;

    expandRulesMgmt() {
	  	if(this.showRules) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandAPIs() {
	  	if(this.showAPIs) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	infrastructureGo() {
        window.open('https://portal.azure.com','_blank');
    }

	expandRule() {
	  	if(this.showRule) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule2() {
	  	if(this.showRule2) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule3() {
	  	if(this.showRule3) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule4() {
	  	if(this.showRule4) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule5() {
	  	if(this.showRule5) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule6() {
	  	if(this.showRule6) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule7() {
	  	if(this.showRule7) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule8() {
	  	if(this.showRule8) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule9() {
	  	if(this.showRule9) {
	      return "block";
	    } else {
	      return "";
	    }
	};

	expandRule10() {
	  	if(this.showRule10) {
	      return "block";
	    } else {
	      return "";
	    }
	};
}
