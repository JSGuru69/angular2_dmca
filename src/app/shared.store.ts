import {Injectable} from '@angular/core';


@Injectable()
export class SharedStoreService {
    static instance: SharedStoreService;

    private validList:any =[];
    private totalCount:any;
    private validCount:any;
    private invlaidCount:any;
   
   constructor(){
       return SharedStoreService.instance = SharedStoreService.instance || this;
   }

   setValidList(list){
       this.validList = list;
   } 
   getValidList(){
       return this.validList;
   }

   setTotalCount(count){
       this.totalCount = count;
   }
   getTotalCount(){
       return this.totalCount;
   }

   setValidCount(count){
       this.validCount = count;
   }
   getValidCount(){
       return this.validCount;
   }

   setInvalidCount(count){
       this.invlaidCount = count;
   }
   getInvalidCount(){
       return this.invlaidCount;
   }
} 
