import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'sidenav',
    templateUrl: './sidenav.html',
    styleUrls: ['../../main.scss']
})

export class SideNavigation {
    navHeight: Number = window.innerHeight;
    searchItem: Boolean;
    constructor(private router: Router) { }
    flag: String = 'one'
    checkFlag(flag) {
        return this.flag == flag ? "active-nav" : "";
    }
    navigation(route, flag) {
        this.router.navigate([route]);
    }
    dashboardClick() {
        this.searchItem = false;
        console.log('working!');
    }
}