import { Component } from '@angular/core';

@Component({
    selector: 'dmca-header',
    templateUrl: './header.component.html',
    styleUrls: ['../../main.scss']
})
export class HeaderComponent {
    private logo = require("assets/pwc-logo.png");
    constructor() {};
}