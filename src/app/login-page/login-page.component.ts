import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';
import { environment } from './../../environments/environment';

const appConfig = require('../../../appConfig.js');

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})



export class LoginPageComponent {

  private environmentName = environment.name;
    Username: any;
    Password: any;
    errorMsg: any;
  constructor(private router: Router, private http: Http) {
  }

  authLogin() {
    let self = this;
    var url;
    if(this.environmentName === 'dev')
      // url = 'http://localhost:4200/auth'
      window.location.assign(appConfig.devDNSName + ':' + appConfig.devPort + '/auth');
    if(this.environmentName === 'prod')
      window.location.assign(appConfig.nodeDNSName + ':' + appConfig.nodePort + '/auth');
  }

  ngOnInit() {
  }

}
