const express = require('express');
const request = require('request');
var formData = require('form-data');
var fs = require('fs');
var multer = require('multer');
var upload = multer({
  dest: 'uploads/',
  limits: {
    fileSize: 10000000
  }
});
var type = upload.single('file');
async = require('async');
var config = require('../../appConfig');
var ip = require('ip');
var http = require("https");
var streamifier = require('streamifier');
var zlib = require('zlib');
var Zip = require("adm-zip");
var fstream = require('fstream');

const router = express.Router();

//var apiUrl = 'https://elastic.snaplogic.com:443/api/1/rest/slsched/feed/PwC/DMCA/shared/';
var snapLogicUrl = config.snapLogicURL; //'http://10.1.0.22:8090/api/1/rest/feed/PwC/DMCA/shared/';
var elasticUrl = config.elasticURL; //'http://10.1.0.36:9200/';
var nodeJsonPath = config.nodeJSONPath;
var customerCode = config.snapLogicCustomerCode;
var environmentCode = config.snapLogicEnvironmentCode;
var org = config.snapLogicOrg;
var custCode = config.snapLogicCustomerCode;
var envCode = config.snapLogicEnvironmentCode;


/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.post("/validation", type, function (req, res) {
  // console.log(req.body);
  console.log(req.file);
  console.log("File Path: "  + req.file.path);
  console.log("File Size: "  + req.file.size);

  // We convert the incoming ZIP file into GZ file since SnapLogic can handle only GZIP/DEFLATE formats only
  // The contents are stored in a Buffer variable which will be later streamed to a file and sent as payload
  var buf = Buffer.from('');
  var zip = new Zip(req.file.path);
  var gzFileName = nodeJsonPath + 'bulk_upload_request_' + new Date().toISOString().replace(/T/, '_').replace(/\..+/, '').replace(/:/,'-').replace(/:/,'-') + '.gz';

  zip.getEntries().forEach(function(entry) {
      var entryName = entry.entryName;
      var decompressedData = zip.readFile(entry);
      buf = Buffer.concat([buf , decompressedData]);
  });


  var options = {
    "method": "POST",
    "hostname": config.snapLogicPublicDNS,
    "port": config.snapLogicPublicPort,
    "path": "/api/1/rest/slsched/feed/" + org + "/" + custCode + "/shared/" + custCode + "-" + envCode + "-" + "MANUAL-VALIDATE-API-EP",
    "headers": {
      "authorization": "Bearer " + config.snapLogicToken,
      "cache-control": "no-cache"
    }
  };

  console.log("POST Options: "  + JSON.stringify(options));

  var post_request = http.request(options, function (apires) {
    var chunks = [];

    apires.on("data", function (chunk) {
      chunks.push(chunk);
    });

    apires.on("error", function (apierr) {
      console.log('Error occurred while invoking API! ' + apierr );
      res.send(apierr);
    });

    apires.on("end", function () {
      var gzipResp = Buffer.concat(chunks);
      var body = zlib.gunzipSync(gzipResp);
      console.log('API Response: ' + body.toString());
      var respArray = JSON.parse(body);
      var totalNotifications = respArray.length;
      var validNotifications = 0;
      respArray.forEach(function(value){
              if (value.validity == 1) validNotifications++;
      });
      var invalidNotifications = totalNotifications - validNotifications;
      console.log('Total: ' + totalNotifications);
      console.log('Valid: ' + validNotifications);
      console.log('Invalid: ' + invalidNotifications);

      // var jsonFileName = nodeJsonPath + 'bulk_upload_response_' + new Date().toISOString().replace(/T/, '_').replace(/\..+/, '').replace(/:/,'-').replace(/:/,'-') + '.json';
      var gzRespFileName = nodeJsonPath + 'bulk_upload_response_' + new Date().toISOString().replace(/T/, '_').replace(/\..+/, '').replace(/:/,'-').replace(/:/,'-') + '.gz';

      // fs.writeFile(jsonFileName, JSON.stringify(respArray), function (err) {
      fs.writeFile(gzRespFileName, gzipResp, function (err) {
        if (err) return console.log(err);
        // console.log('Successfully saved the API response to ' + jsonFileName);
        console.log('Successfully saved the API response to ' + gzRespFileName);
        var nodeIPAddress = ip.address();
        console.log('Node Host IP: ' + nodeIPAddress);
        var notifyResponse = {
          totalCount: totalNotifications,
          validCount: validNotifications,
          invalidCount: invalidNotifications,
          // file_path: jsonFileName,
          file_path: gzRespFileName,
          node_host: nodeIPAddress
        };
        fs.unlinkSync(req.file.path);
        fs.unlinkSync(gzFileName);
        console.log('UI Response: ' + JSON.stringify(notifyResponse));
        res.send(notifyResponse);
      });
    });
  });

  // post_request.write(fs.readFileSync(req.file.path).toString());
  // post_request.end();

  var writeReq = fstream.Writer({ 'path': gzFileName });

  writeReq.on('close', function() {
    console.log('API File Payload: ' + gzFileName);
    post_request.write(fs.readFileSync(gzFileName));
    post_request.end();
  });

  streamifier.createReadStream(buf).pipe(zlib.Gzip()).pipe(writeReq);
  
  /**************************************************************************************************
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////    
  // The following request post method is creating issues when the number of rows in CSV is > 3928. 
  // Hence commented and replaced by Native HTTP POST
  ///////////////////////////////////////////////////////////////////////////////////////////////////    
  
  //var data = {file:fs.createReadStream(req.file.path)}
  var options = {
    url: snapLogicUrl + customerCode + '-' + environmentCode + '-MANUAL-VALIDATE-API-EP',
    headers: {
      'Authorization': 'Bearer ' + config.snapLogicToken.toString(),
      //'cache-control': 'no-cache',
      'encoding': 'null',
      "Content-Type": "application/octet-stream"
    }
  };
  //options.body = fs.readFileSync(req.file.path); - this also works!!

  fs.createReadStream(req.file.path).pipe(
    request.post(options, function (err, resp, body) {
      if (err) {
        console.log('Error!');
        res.send(err);
      } else {
        console.log('Response: ' + resp.body.toString());
        // fs.unlinkSync(req.file.path); //Test this later
        // res.send(JSON.parse(resp.body));

        var respArray = JSON.parse(resp.body);
        var totalNotifications = respArray.length;
        var validNotifications = 0;
        respArray.forEach(function(value){
                if (value.validity == 1) validNotifications++;
        });
        var invalidNotifications = totalNotifications - validNotifications;
        console.log('Total: ' + totalNotifications);
        console.log('Valid: ' + validNotifications);
        console.log('Invalid: ' + invalidNotifications);

        var jsonFileName = nodeJsonPath + 'bulk_upload_response_' + new Date().toISOString().replace(/T/, '_').replace(/\..+/, '').replace(/:/,'-').replace(/:/,'-') + '.json';
        fs.writeFile(jsonFileName, JSON.stringify(respArray), function (err) {
                if (err) return console.log(err);
                console.log('Successfully saved the API response to ' + jsonFileName);
                var nodeIPAddress = ip.address();
                console.log('Host: ' + nodeIPAddress);
                var notifyResponse = {
                        totalCount: totalNotifications,
                        validCount: validNotifications,
                        invalidCount: invalidNotifications,
                        file_path: jsonFileName,
                        node_host: nodeIPAddress
                        };
                fs.unlinkSync(req.file.path);
                console.log('UI Response: ' + JSON.stringify(notifyResponse));
                res.send(notifyResponse);
        });
      }
    })
  );
  **********************************************************************************************************************************/

});

router.post("/upload", (req, res) => {
  console.log("upload");
  var options = {
    url: snapLogicUrl + customerCode + '-' + environmentCode + '-MANUAL-UPLOAD-API-EP',
    headers: {
      'Authorization': 'Bearer ' + config.snapLogicToken.toString(),
      //'cache-control': 'no-cache',
      //'encoding':'null',
      "Content-Type": "application/json"
    },
    body: req.body,
    json: true
  };
  console.log("Options", options);
  request.post(options, function (err, resp, body) {
    if (err) {
      console.log('Error!');
      res.send(err);
    } else {
      console.log('Deleting file: ' + JSON.stringify(req.body[0]));
      fs.unlinkSync(req.body[0].file_path);
      console.log('Response: ' + JSON.stringify(resp.body));
      res.send(resp.body);
    }
  })
})

router.post("/manual", (req, res) => {
  console.log("manual", req.body);
  var temp = [req.body];
  var jsonBody = JSON.stringify(temp);
  console.log("manual", jsonBody);
  var options = {
    url: snapLogicUrl + customerCode + '-' + environmentCode + '-WEBFORM-UPLOAD-API-EP',
    headers: {
      'Authorization': 'Bearer ' + config.snapLogicToken.toString(),
      "Content-Type": "application/json"
    },
    body: req.body,
    json: true
  };
  console.log("Options", options);
  request.post(options, function (err, resp, body) {
    if (err) {
      console.log('Error!');
      res.send(err);
    } else {
      console.log('Response: ' + resp.body.toString());
      res.send(resp.body);
    }
  })
})

router.get("/notificationsupload", (req, res) => {
  console.log("Total notificationsupload");

  var options = {
    url: elasticUrl + 'charts/notification_uploads/_search?size=10000&filter_path=hits.hits._source&sort=year_month&pretty',
    headers: {
      'content-type': 'application/json',
      'Authorization': 'Basic ' + config.elasticToken.toString()
    },
  };
  console.log("Options", options);
  request.get(options, function (err, resp, body) {
    if (err) {
      console.log('Error!');
      res.send(err);
    } else {
      console.log('Response: ' + resp.body.toString());
      res.send(resp.body);
    }
  })
})

router.get("/subscriberselected/:subId", (req, res) => {
  console.log("subscriberselected");
  var id = req.params.subId.toString();
  console.log("ID: ", id);
  async.parallel({
    subscriberDetails: function (callback) {
      console.log("subscriber details");
      var dataString = {"sort":[{"infringement_level":{"order":"desc"}}],"query":{"bool":{"must":{"term":{"id":id}}}}}
      var options = {
        url: elasticUrl + 'subscribers/details/_search?filter_path=hits.hits._source&size=10000&pretty',
        body: dataString,
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
        json: true
      };
      console.log("subscriber details Options", options);
      request.post(options, function (err, resp) {
        console.log('subscriber details Response: ' + resp.body);
        return callback(err, resp.body);
      })
    },
    violationhistory: function (callback) {
      console.log("infringementHistory");
var dataString = {"sort":[{"notification_date":{"order":"asc"}}],"query":{"bool":{"must":{"term":{"id":id}}}}}
      var options = {
        url: elasticUrl + 'subscribers/history/_search?filter_path=hits.hits._source.infringement_level,hits.hits._source.notification_date&size=10000&pretty',
        body: dataString,
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
        json: true
      };
      console.log("violationhistory Options", options);
      request.post(options, function (err, resp) {
        console.log('violationhistory Response: ' + resp.body);
        return callback(err, resp.body);
      })
    }
  }, function (err, finalResult) {
    if (err) {
      console.log(err);
      return res.send(err);
    }
    var response = finalResult;
    console.log("Sub details Result:", response);
    return res.json(response);
  });
})

router.get("/autocomplete/:search", (req, res) => {
  console.log("autocomplete search string", req.params.search.toString());
  var searchString = req.params.search.toString();
  var idString = searchString.toUpperCase();
  var dataString = '{"sort":[{"level":{"order":"desc"}}],"query":{"bool":{"should":[{"wildcard":{"id":"*' + idString + '*"}},{"wildcard":{"name":"*' + searchString + '*"}},{"wildcard":{"wan_ip":"*' + searchString + '*"}}]}}}';

  var options = {
    url: elasticUrl + 'subscribers/infringements/_search?filter_path=hits.hits._source&size=10000',
    body: dataString,
    headers: {
      'content-type': 'application/json',
      'Authorization': 'Basic ' + config.elasticToken.toString()
    },
  };
  console.log("Options", options);
  request.post(options, function (err, resp, body) {
    if (err) {
      console.log('Error!');
      res.send(err);
    } else {
      console.log('Response: ' + resp.body.toString());
      var result = JSON.parse(resp.body.toString());
      res.send(result);
    }
  })
})

router.get("/dashboard", (req, res) => {
  console.log("In dashboard API");
  var totalNoticesApi = elasticUrl + 'charts/notices_terminations/_search?filter_path=hits.hits._source&size=10000&q=period_type:';
  var totalNoticesChartApi = elasticUrl + 'charts/noticeschart/_search?filter_path=hits.hits._source&size=10000&sort=row_id&q=period_type:'
  var contentOwnerApi = elasticUrl + 'charts/topownerlist/_search?filter_path=hits.hits._source&size=10000&q=period_type:';
  var serviceTypeApi = elasticUrl + 'charts/servicetypes/_search?filter_path=hits.hits._source&size=10000&q=period_type:';
  var violationApi = elasticUrl + 'charts/avgperlevel_mitigation/_search?filter_path=hits.hits._source&size=10000&sort=infringement_level_value:desc&q=period_type:';
  var totalUploadEventApi = elasticUrl + 'charts/noticetrends/_search?filter_path=hits.hits._source&size=10000&q=period_type:';
  async.parallel({
    //YEAR
    //Total Notices - Numbers
    totalNoticesYTD: function (callback) {
      console.log("totalnotices");
      var options = {
        url: totalNoticesApi + 'YTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Total Notices - Line graph
    totalNoticesChartYTD: function (callback) {
      console.log("totalnotices chart");
      var options = {
        url: totalNoticesChartApi + 'YTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices chart Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices chart Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Top 5 Content Owner Infringements list
    contentOwnerYTD: function (callback) {
      console.log("contentowner");
      var options = {
        url: contentOwnerApi + 'YTD&sort=total_notices:desc&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Content owner Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Content owner Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //METHOD OF VIOLATION chart
    serviceTypeYTD: function (callback) {
      console.log("Service type");
      var options = {
        url: serviceTypeApi + 'YTD&sort=total_notices:desc&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Service Type Options", options);
      request.get(options, function (err, resp) {
        console.log('Service Type Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Violation Chart
    violationYTD: function (callback) {
      console.log("violation aggregate");
      var options = {
        url: violationApi + 'YTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Violation aggregate Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Violation aggregate Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //NOTICE TOTAL UPLOAD EVENTS chart
    totalUploadEventsYTD: function (callback) {
      console.log("Total violations");

      var options = {
        url: totalUploadEventApi + 'YTD&sort=row_id',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total violations Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total violations Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //DAY APIS
    //Total Notices - Numbers
    totalNoticesDTD: function (callback) {
      console.log("totalnotices");
      var options = {
        url: totalNoticesApi + 'DTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Total Notices - Line graph
    totalNoticesChartDTD: function (callback) {
      console.log("totalnotices chart");
      var options = {
        url: totalNoticesChartApi + 'DTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices chart Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices chart Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Top 5 Content Owner Infringements list - currently QTD
    contentOwnerDTD: function (callback) {
      console.log("contentowner");
      var options = {
        url: contentOwnerApi + 'DTD&sort=total_notices:desc&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Content owner Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Content owner Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //METHOD OF VIOLATION chart - currently QTD
    serviceTypeDTD: function (callback) {
      console.log("Service type");
      var options = {
        url: serviceTypeApi + 'DTD&sort=total_notices:desc&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Service Type Options", options);
      request.get(options, function (err, resp) {
        console.log('Service Type Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Violation chart - currently QTD
    violationDTD: function (callback) {
      console.log("violation aggregate");
      var options = {
        url: violationApi + 'DTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Violation aggregate Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Violation aggregate Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //NOTICE TOTAL UPLOAD EVENTS chart
    totalUploadEventsDTD: function (callback) {
      console.log("Total violations");

      var options = {
        url: totalUploadEventApi + 'DTD&sort=row_id',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total violations Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total violations Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },

    //Month APIS
    //Total Notices - Numbers
    totalNoticesMTD: function (callback) {
      console.log("totalnotices");
      var options = {
        url: totalNoticesApi + 'MTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Total Notices - Line graph
    totalNoticesChartMTD: function (callback) {
      console.log("totalnotices chart");
      var options = {
        url: totalNoticesChartApi + 'MTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices chart Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices chart Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Top 5 Content Owner Infringements list - currently QTD
    contentOwnerMTD: function (callback) {
      console.log("contentowner");
      var options = {
        url: contentOwnerApi + 'MTD&sort=total_notices:desc&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Content owner Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Content owner Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //METHOD OF VIOLATION chart - currently QTD
    serviceTypeMTD: function (callback) {
      console.log("Service type");
      var options = {
        url: serviceTypeApi + 'MTD&sort=total_notices:desc&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Service Type Options", options);
      request.get(options, function (err, resp) {
        console.log('Service Type Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Violation chart - currently QTD
    violationMTD: function (callback) {
      console.log("violation aggregate");
      var options = {
        url: violationApi + 'MTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Violation aggregate Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Violation aggregate Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //NOTICE TOTAL UPLOAD EVENTS chart
    totalUploadEventsMTD: function (callback) {
      console.log("Total violations");
      var options = {
        url: totalUploadEventApi + 'MTD&sort=row_id',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total violations Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total violations Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },

    //Q APIS
    //Total Notices - Numbers
    totalNoticesQTD: function (callback) {
      console.log("totalnotices");
      var options = {
        url: totalNoticesApi + 'QTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Total Notices - Line graph
    totalNoticesChartQTD: function (callback) {
      console.log("totalnotices chart");
      var options = {
        url: totalNoticesChartApi + 'QTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices chart Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices chart Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Top 5 Content Owner Infringements list - currently QTD
    contentOwnerQTD: function (callback) {
      console.log("contentowner");
      var options = {
        url: contentOwnerApi + 'QTD&sort=total_notices:desc&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Content owner Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Content owner Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //METHOD OF VIOLATION chart - currently QTD
    serviceTypeQTD: function (callback) {
      console.log("Service type");
      var options = {
        url: serviceTypeApi + 'QTD&sort=total_notices:desc&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Service Type Options", options);
      request.get(options, function (err, resp) {
        console.log('Service Type Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Violation chart - currently QTD
    violationQTD: function (callback) {
      console.log("violation aggregate");
      var options = {
        url: violationApi + 'QTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Violation aggregate Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Violation aggregate Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //NOTICE TOTAL UPLOAD EVENTS chart
    totalUploadEventsQTD: function (callback) {
      console.log("Total violations");

      var options = {
        url: totalUploadEventApi + 'QTD&sort=row_id',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total violations Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total violations Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    }
  }, function (err, finalResult) {
    if (err) {
      console.log(err);
      return res.send(err);
    }
    var response = finalResult;
    return res.send(response);
  });
})

router.get("/ogcbusiness", (req, res) => {
  console.log("In OGC dashboard API");
  async.parallel({
    //Total Notices - Numbers
    totalNoticesDTD: function (callback) {
      console.log("totalnotices");
      var options = {
        url: elasticUrl + 'charts/notices_terminations/_search?filter_path=hits.hits._source&size=10000&q=period_type:YTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices Options", options);
      request.get(options, function (err, resp, body) {
        console.log('Total notices Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Total Notices - Line graph
    totalNoticesChartDTD: function (callback) {
      console.log("totalnotices chart");
      var options = {
        url: elasticUrl + 'charts/noticeschart/_search?filter_path=hits.hits._source&size=10000&sort=row_id&q=period_type:YTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Total notices chart Options", options.url);
      request.get(options, function (err, resp, body) {
        console.log('Total notices chart Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    },
    //Average Infringements / Level & Mitigation Success chart data
    chartsDTD: function (callback) {
      console.log("Average Infringements / Level & Mitigation Success chart data chart");
      var options = {
        url: elasticUrl + 'charts/avgperlevel_mitigation/_search?filter_path=hits.hits._source&size=10000&sort=infringement_level_value:desc&q=period_type:YTD&pretty',
        headers: {
          'content-type': 'application/json',
          'Authorization': 'Basic ' + config.elasticToken.toString()
        },
      };
      console.log("Average Infringements / Level & Mitigation Success chart data Options", options.url);
      request.get(options, function (err, resp, body) {
        console.log('Average Infringements / Level & Mitigation Success chart data Response: ' + resp.body.toString());
        return callback(err, resp.body);
      })
    }
  }, function (err, finalResult) {
    if (err) {
      console.log(err);
      return res.send(err);
    }
    var response = finalResult;
    return res.send(response);
  });
})

router.post("/subscriber/action", (req, res) => {
  console.log("User action", req.body);
  var options = {
    url: snapLogicUrl + customerCode + '-' + environmentCode + '-SUBAPP-ACTION-API-EP',
    headers: {
      'Authorization': 'Bearer ' + config.snapLogicToken.toString(),
      'content-type': 'application/json'
    },
    body: req.body,
    json: true
  };
  console.log("Options", options.url, options.body);
  request.post(options, function (err, resp, body) {
    if (err) {
      console.log('Error!');
      res.send(err);
    } else {
      console.log('Response: ' + resp.body.toString());
      res.send(resp.body);
    }
  })
});

// function mask(value) {
//     let maskedValue = "";
//     for (let i = 0; i < value.length; i++) {
//         maskedValue += "*";
//     }
//     return maskedValue;
// };

// Object.keys(myObject).forEach(key => {
//     output[key] = mask(myObject[key]);
// });

module.exports = router;
